/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "config.h"

#include <stddef.h>

#include "util-macros.h"
#include "util-mem.h"

#include "brei-shared.h"
#include "libeis-private.h"

/* The message type for the wire format */
enum message_type {
	MESSAGE_CONNECT,
	MESSAGE_DISCONNECT,
	MESSAGE_ADD_DEVICE,
	MESSAGE_REMOVE_DEVICE,
	MESSAGE_POINTER_REL,
	MESSAGE_POINTER_ABS,
	MESSAGE_POINTER_BUTTON,
	MESSAGE_KEYBOARD_KEY,
	MESSAGE_TOUCH,

	MESSAGE_CONFIGURE_NAME,
	MESSAGE_CONFIGURE_CAPABILITIES,
};

static inline const char *
message_type_to_string(enum message_type type)
{
	switch(type) {
	CASE_RETURN_STRING(MESSAGE_CONNECT);
	CASE_RETURN_STRING(MESSAGE_DISCONNECT);
	CASE_RETURN_STRING(MESSAGE_ADD_DEVICE);
	CASE_RETURN_STRING(MESSAGE_REMOVE_DEVICE);
	CASE_RETURN_STRING(MESSAGE_POINTER_REL);
	CASE_RETURN_STRING(MESSAGE_POINTER_ABS);
	CASE_RETURN_STRING(MESSAGE_POINTER_BUTTON);
	CASE_RETURN_STRING(MESSAGE_KEYBOARD_KEY);
	CASE_RETURN_STRING(MESSAGE_TOUCH);
	CASE_RETURN_STRING(MESSAGE_CONFIGURE_NAME);
	CASE_RETURN_STRING(MESSAGE_CONFIGURE_CAPABILITIES);
	}
	assert(!"Unimplemented message type");
}

struct message {
	enum message_type type;
	union {
		struct message_connect {
			char *name;
		} connect;
		struct message_disconnect {
			uint8_t pad; /* no data */
		} disconnect;
		struct message_add_device {
			uint32_t deviceid;
			char *name;
			uint32_t capabilities;
			struct dimensions dim_pointer;
			struct dimensions dim_touch;
			/* keymap is passed as fd if type isn't none */
			enum eis_keymap_type keymap_type;
			int keymap_fd;
			size_t keymap_size;
			uint32_t seat;
		} add_device;
		struct message_remove_device {
			uint32_t deviceid;
		} remove_device;
		struct message_pointer_rel {
			uint32_t deviceid;
			double x;
			double y;
		} pointer_rel;
		struct message_pointer_abs {
			uint32_t deviceid;
			double x;
			double y;
		} pointer_abs;
		struct message_pointer_button {
			uint32_t deviceid;
			uint32_t button;
			bool state;
		} pointer_button;
		struct message_keyboard_key {
			uint32_t deviceid;
			uint32_t key;
			bool state;
		} keyboard_key;
		struct message_touch {
			uint32_t deviceid;
			uint32_t touchid;
			bool is_down;
			bool is_up;
			double x;
			double y;
		} touch;
		struct message_configure_name {
			char *name;
		} configure_name;
		struct message_configure_capabilities {
			bool policy_is_allow;
			uint32_t caps_allow;
			uint32_t caps_deny;
		} configure_caps;
	};
};

void message_free(struct message *msg);

DEFINE_TRIVIAL_CLEANUP_FUNC(struct message*, message_free);
#define _cleanup_message_ _cleanup_(message_freep)

int
eis_proto_send_disconnect(struct eis_client *client);

int
eis_proto_send_connect(struct eis_client *client);

int
eis_proto_send_seat_added(struct eis_client *client,
			  struct eis_seat *seat);

int
eis_proto_send_seat_removed(struct eis_client *client,
			    struct eis_seat *seat);

int
eis_proto_send_device_added(struct eis_client *client,
			   struct eis_device *device);

int
eis_proto_send_device_removed(struct eis_client *client,
			     struct eis_device *device);

int
eis_proto_send_device_suspended(struct eis_client *client,
				struct eis_device *device);

int
eis_proto_send_device_resumed(struct eis_client *client,
			      struct eis_device *device);

struct message *
eis_proto_parse_message(struct brei_message *msg, size_t *consumed);
