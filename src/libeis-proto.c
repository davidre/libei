/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <stdbool.h>

#include "util-object.h"
#include "util-io.h"
#include "util-strings.h"
#include "util-tristate.h"

#include "proto/ei.pb-c.h"
#include "libeis-proto.h"
#include "brei-shared.h"

void
message_free(struct message *msg)
{
	switch (msg->type) {
	case MESSAGE_ADD_DEVICE:
		free(msg->add_device.name);
		break;
	case MESSAGE_CONNECT:
		free(msg->connect.name);
		break;
	case MESSAGE_CONFIGURE_NAME:
		free(msg->configure_name.name);
		break;
	default:
		break;
	}
	free(msg);
}

static inline void
log_wire_message(struct eis *eis, const ServerMessage *msg)
{
	const char *message = NULL;

#define MSG_STRING_CASE(_name) \
	case SERVER_MESSAGE__MSG_##_name: message = #_name; break;

	switch (msg->msg_case) {
		case SERVER_MESSAGE__MSG__NOT_SET:
			abort();
		MSG_STRING_CASE(CONNECTED);
		MSG_STRING_CASE(DISCONNECTED);
		MSG_STRING_CASE(SEAT_ADDED);
		MSG_STRING_CASE(SEAT_REMOVED);
		MSG_STRING_CASE(DEVICE_ADDED);
		MSG_STRING_CASE(DEVICE_REMOVED);
		MSG_STRING_CASE(DEVICE_RESUMED);
		MSG_STRING_CASE(DEVICE_SUSPENDED);
			break;
		default:
			assert(!"Unimplemented message type");
			break;
	}
	log_debug(eis, "sending wire message %s\n", message);

#undef MSG_STRING_CASE
}

static int
eis_proto_send_msg(struct eis_client *client, const ServerMessage *msg)
{
	log_wire_message(eis_client_get_context(client), msg);

	size_t msglen = server_message__get_packed_size(msg);
	Frame frame = FRAME__INIT;
	frame.length = msglen;
	size_t framelen = frame__get_packed_size(&frame);

	uint8_t buf[framelen + msglen];
	frame__pack(&frame, buf);
	server_message__pack(msg, buf + framelen);
	return min(0, xsend(source_get_fd(client->source), buf, sizeof(buf)));
}

static int
eis_proto_send_msg_with_fds(struct eis_client *client, const ServerMessage *msg, int *fds)
{
	log_wire_message(eis_client_get_context(client), msg);

	size_t msglen = server_message__get_packed_size(msg);
	Frame frame = FRAME__INIT;
	frame.length = msglen;
	size_t framelen = frame__get_packed_size(&frame);

	uint8_t buf[framelen + msglen];
	frame__pack(&frame, buf);
	server_message__pack(msg, buf + framelen);

	return min(0, xsend_with_fd(source_get_fd(client->source), buf, sizeof(buf), fds));
}

int
eis_proto_send_disconnect(struct eis_client *client)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	Disconnected disconnected = DISCONNECTED__INIT;

	msg.disconnected = &disconnected;
	msg.msg_case = SERVER_MESSAGE__MSG_DISCONNECTED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_connect(struct eis_client *client)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	Connected connected = CONNECTED__INIT;

	msg.connected = &connected;
	msg.msg_case = SERVER_MESSAGE__MSG_CONNECTED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_seat_added(struct eis_client *client,
			  struct eis_seat *seat)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	SeatAdded added = SEAT_ADDED__INIT;

	added.seatid = seat->id;
	added.name = seat->name;
	added.capabilities = seat->capabilities_mask;

	msg.seat_added = &added;
	msg.msg_case = SERVER_MESSAGE__MSG_SEAT_ADDED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_seat_removed(struct eis_client *client,
			    struct eis_seat *seat)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	SeatRemoved removed = SEAT_REMOVED__INIT;

	removed.seatid = seat->id;

	msg.seat_removed = &removed;
	msg.msg_case = SERVER_MESSAGE__MSG_SEAT_REMOVED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_device_added(struct eis_client *client, struct eis_device *device)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	DeviceAdded added = DEVICE_ADDED__INIT;
	struct eis_seat *seat = eis_device_get_seat(device);
	int fds[2] = {-1, -1};

	added.deviceid = device->id;
	added.name = device->name;
	added.capabilities = device->capabilities;

	if (device->keymap) {
		struct eis_keymap *keymap = device->keymap;
		added.keymap_type = keymap->type;
		added.keymap_size = keymap->size;
		added.keymap_from_server = !keymap->is_client_keymap;
		if (!keymap->is_client_keymap)
			fds[0] = keymap->fd;
	} else {
		/* it's NULL anyway */
		added.keymap_from_server = true;
	}
	added.seatid = seat->id;

	msg.device_added = &added;
	msg.msg_case = SERVER_MESSAGE__MSG_DEVICE_ADDED;

	return eis_proto_send_msg_with_fds(client, &msg, fds);
}

int
eis_proto_send_device_removed(struct eis_client *client, struct eis_device *device)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	DeviceRemoved removed = DEVICE_REMOVED__INIT;

	removed.deviceid = device->id;

	msg.device_removed = &removed;
	msg.msg_case = SERVER_MESSAGE__MSG_DEVICE_REMOVED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_device_suspended(struct eis_client *client, struct eis_device *device)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	DeviceSuspended suspended = DEVICE_SUSPENDED__INIT;

	suspended.deviceid = device->id;

	msg.device_suspended = &suspended;
	msg.msg_case = SERVER_MESSAGE__MSG_DEVICE_SUSPENDED;

	return eis_proto_send_msg(client, &msg);
}

int
eis_proto_send_device_resumed(struct eis_client *client, struct eis_device *device)
{
	ServerMessage msg = SERVER_MESSAGE__INIT;
	DeviceResumed resumed = DEVICE_RESUMED__INIT;

	resumed.deviceid = device->id;

	msg.device_resumed = &resumed;
	msg.msg_case = SERVER_MESSAGE__MSG_DEVICE_RESUMED;

	return eis_proto_send_msg(client, &msg);
}

struct message *
eis_proto_parse_message(struct brei_message *bmsg, size_t *consumed)
{
	_cleanup_(message_freep) struct message *msg = xalloc(sizeof(*msg));
	ClientMessage *proto = client_message__unpack(NULL, bmsg->len,
						      (const unsigned char*)bmsg->data);
	if (!proto) {
		return NULL;
	}

	*consumed = bmsg->len;

	bool success = true;
	switch (proto->msg_case) {
	case CLIENT_MESSAGE__MSG_CONNECT:
		{
			Connect *c = proto->connect;
			*msg = (struct message) {
				.type = MESSAGE_CONNECT,
				.connect.name = xstrdup(c->name),
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_DISCONNECT:
		*msg = (struct message) {
			.type = MESSAGE_DISCONNECT,
		};
		break;
	case CLIENT_MESSAGE__MSG_ADD_DEVICE:
		{
			AddDevice *a = proto->add_device;
			*msg = (struct message) {
				.type = MESSAGE_ADD_DEVICE,
				.add_device.deviceid = a->deviceid,
				.add_device.name = a->name[0] ? xstrdup(a->name) : NULL,
				.add_device.capabilities = a->capabilities,
				.add_device.dim_pointer.width = a->pointer_width,
				.add_device.dim_pointer.height = a->pointer_height,
				.add_device.dim_touch.width = a->touch_width,
				.add_device.dim_touch.height = a->touch_height,
				.add_device.keymap_fd = -1,
				.add_device.keymap_type = a->keymap_type,
				.add_device.keymap_size = a->keymap_size,
				.add_device.seat = a->seat,
			};

			if (a->keymap_type)
				msg->add_device.keymap_fd = brei_message_take_fd(bmsg);
		}
		break;
	case CLIENT_MESSAGE__MSG_REMOVE_DEVICE:
		{
			RemoveDevice *r = proto->remove_device;
			*msg = (struct message) {
				.type = MESSAGE_REMOVE_DEVICE,
				.remove_device.deviceid = r->deviceid,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_REL:
		{
			PointerRelative *r = proto->rel;
			*msg = (struct message) {
				.type = MESSAGE_POINTER_REL,
				.pointer_rel.deviceid = r->deviceid,
				.pointer_rel.x = r->x,
				.pointer_rel.y = r->y,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_ABS:
		{
			PointerAbsolute *a = proto->abs;
			*msg = (struct message) {
				.type = MESSAGE_POINTER_ABS,
				.pointer_abs.deviceid = a->deviceid,
				.pointer_abs.x = a->x,
				.pointer_abs.y = a->y,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_BUTTON:
		{
			PointerButton *b = proto->button;
			*msg = (struct message) {
				.type = MESSAGE_POINTER_BUTTON,
				.pointer_button.deviceid = b->deviceid,
				.pointer_button.button = b->button,
				.pointer_button.state = b->state,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_KEY:
		{
			KeyboardKey *k = proto->key;
			*msg = (struct message) {
				.type = MESSAGE_KEYBOARD_KEY,
				.keyboard_key.deviceid = k->deviceid,
				.keyboard_key.key = k->key,
				.keyboard_key.state = k->state,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_TOUCH:
		{
			Touch *t = proto->touch;
			*msg = (struct message) {
				.type = MESSAGE_TOUCH,
				.touch.deviceid = t->deviceid,
				.touch.touchid = t->touchid,
				.touch.is_down = t->is_down,
				.touch.is_up = t->is_up,
				.touch.x = t->x,
				.touch.y = t->y,
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_CONFIGURE_NAME:
		{
			ConfigureName *c = proto->configure_name;
			*msg = (struct message) {
				.type = MESSAGE_CONFIGURE_NAME,
				.configure_name.name = xstrdup(c->name),
			};
		}
		break;
	case CLIENT_MESSAGE__MSG_CONFIGURE_CAPS:
		{
			ConfigureCapabilities *c = proto->configure_caps;
			*msg = (struct message) {
				.type = MESSAGE_CONFIGURE_CAPABILITIES,
				.configure_caps.policy_is_allow = c->policy_is_allow,
				.configure_caps.caps_allow = c->allowed_capabilities,
				.configure_caps.caps_deny = c->denied_capabilities,
			};
		}
		break;
	default:
		success = false;
		break;
	}

	client_message__free_unpacked(proto, NULL);

	return success ? steal(&msg) : NULL;
}

