/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/**
 * @addtogroup libei EI - The client API
 *
 * libei is the client-side module. This API should be used by processes
 * that need to emulate devices.
 *
 * @{
 */

/**
 * @struct ei
 *
 * The main context to interact with libei. A libei context is a single
 * connection to an EIS implementation and may contain multiple devices, see
 * @ref ei_device.
 *
 * An @ref ei context is refcounted, see ei_unref().
 */
struct ei;

/**
 * @struct ei_device
 *
 * A single device to generate input events from. A device may have multiple
 * capabilities. For example, a single device may be a pointer and a keyboard
 * and a touch device. It is up to the EIS implementation on how to handle
 * this case, some implementations may split a single device up into
 * multiple virtual devices, others may not.
 *
 * An @ref ei_device is refcounted, see ei_device_unref().
 */
struct ei_device;

/**
 * @struct ei_seat
 *
 * A logical seat for a group of devices. Seats are provided by the EIS
 * implementation, devices may be added to a seat. The hierarchy of objects
 * looks like this:
 * <pre>
 *    ei ---- ei_seat one ---- ei_device 1
 *       \                \
 *        \                --- ei device 2
 *         --- ei_seat two --- ei device 3
 * </pre>
 */
struct ei_seat;

/**
 * @struct ei_event
 *
 * An event received from the EIS implementation. See @ref ei_event_type
 * for the list of possible event types.
 *
 * An @ref ei_event is refcounted, see ei_event_unref().
 */
struct ei_event;

/**
 * @struct ei_keymap
 *
 * An keymap for a device with the @ref EI_DEVICE_CAP_KEYBOARD capability.
 *
 * An @ref ei_keymap is refcounted, see ei_keymap_unref().
 */
struct ei_keymap;

/**
 * @enum ei_device_capability
 *
 * The set of supported capabilities. A device may have one or more
 * capabilities but never zero - a EIS implementation must reject any
 * device with zero capabilities.
 *
 * Capabilities are requested by the client but the EIS
 * implementation may further reduce the capabilities. For example, a client
 * may request the pointer and keyboard capability but only the former is
 * permitted by the server. Any keyboard events sent by such a device events
 * will be treated as client bug and discarded.
 *
 * See eis_device_has_capability().
 *
 */
enum ei_device_capability {
	EI_DEVICE_CAP_POINTER = 1,
	EI_DEVICE_CAP_POINTER_ABSOLUTE,
	EI_DEVICE_CAP_KEYBOARD,
	EI_DEVICE_CAP_TOUCH,
};

/**
 * @enum ei_keymap_type
 *
 * The set of supported keymap types for a struct @ref ei_keymap.
 */
enum ei_keymap_type {
	/**
	 * A libxkbcommon-compatible XKB keymap.
	 */
	EI_KEYMAP_TYPE_XKB = 1,
};

/**
 * @enum ei_keymap_source
 *
 * Identifies who provided a struct @ref ei_keymap.
 */
enum ei_keymap_source {
	/**
	 * The keymap is the one provided by the client.
	 */
	EI_KEYMAP_SOURCE_CLIENT = 1,
	/**
	 * The keymap is provided by the server.
	 */
	EI_KEYMAP_SOURCE_SERVER,
};

/**
 * @enum ei_keymap_source
 */
enum ei_event_type {
	/**
	 * The server has approved the connection to this client. The client
	 * may now add devices with ei_device_add(). Where the server does
	 * not approve the connection, @ref EI_EVENT_DISCONNECT is sent
	 * instead.
	 *
	 * This event is only sent once after the initial connection
	 * request.
	 */
	EI_EVENT_CONNECT = 1,
	/**
	 * The server has disconnected this client - all resources left to
	 * reference this server are now obsolete. Once this event has been
	 * received, the struct @ref ei and all its associated resources
	 * should be released.
	 *
	 * This event may occur at any time after the connection has been
	 * made and is the last event to be received by this ei instance.
	 *
	 * libei guarantees that a @ref EI_EVENT_DISCONNECT is provided to
	 * the caller even where the server does not send one.
	 */
	EI_EVENT_DISCONNECT,

	/**
	 * The server has added a seat available to this client.
	 *
	 * libei guarantees that any seat added has a corresponding @ref
	 * EI_EVENT_SEAT_REMOVED event before @ref EI_EVENT_DISCONNECT.
	 * libei guarantees that any device in this seat generates a @ref
	 * EI_EVENT_DEVICE_REMOVED event before the @ref
	 * EI_EVENT_SEAT_REMOVED event.
	 */
	EI_EVENT_SEAT_ADDED,

	/**
	 * The server has removed a seat previously available to this
	 * client. The caller should release the struct @ref ei_seat and
	 * all its associated resources. No devices can be created through
	 * this seat anymore.
	 *
	 * libei guarantees that any device in this seat generates a @ref
	 * EI_EVENT_DEVICE_REMOVED event before the @ref
	 * EI_EVENT_SEAT_REMOVED event.
	 */
	EI_EVENT_SEAT_REMOVED,

	/**
	 * The server has added a device for this client. The capabilities
	 * of the device may not match the requested capabilities - it is up
	 * to the client to verify the minimum required capabilities are
	 * indeed set.
	 *
	 * Where the server refuses to add a specific device or the
	 * intersection of requested capabilities and allowed capabilities
	 * is zero, the server sends @ref EI_EVENT_DEVICE_REMOVED instead.
	 *
	 * libei guarantees that any device added has a corresponding @ref
	 * EI_EVENT_DEVICE_REMOVED event before @ref EI_EVENT_DISCONNECT.
	 */
	EI_EVENT_DEVICE_ADDED,

	/**
	 * The server has removed a device belonging to this client. The
	 * caller should release the struct @ref ei_device and all its
	 * associated resources. Any events sent through a removed device
	 * are discarded.
	 *
	 * When this event is received, the device is already removed. A
	 * caller does not need to call ei_device_remove() event on this
	 * device.
	 */
	EI_EVENT_DEVICE_REMOVED,

	/**
	 * Any events sent from this device will be discarded until the next
	 * resume.
	 */
	EI_EVENT_DEVICE_SUSPENDED,
	/**
	 * The client may send events.
	 */
	EI_EVENT_DEVICE_RESUMED,
};

/**
 * Create a new ei context. The context is refcounted and must be released
 * with ei_unref().
 *
 * A context supports exactly one backend, set up with one of
 * ei_setup_backend_socket() or ei_setup_backend_fd().
 *
 * @param user_data An opaque pointer to be returned with ei_get_user_data()
 *
 * @see ei_set_user_data
 * @see ei_get_user_data
 * @see ei_setup_backend_fd
 * @see ei_setup_backend_socket
 */
struct ei *
ei_new(void *user_data);

enum ei_log_priority {
	EI_LOG_PRIORITY_DEBUG = 10,
	EI_LOG_PRIORITY_INFO = 20,
	EI_LOG_PRIORITY_WARNING = 30,
	EI_LOG_PRIORITY_ERROR = 40,
};

/**
 * The log handler for library logging. This handler is only called for
 * messages with a log level equal or greater than than the one set in
 * ei_log_set_priority().
 *
 * @param message The log message as a null-terminated string
 * @param is_continuation The message is a continuation of the previous
 * message. The caller should skip any per-line-based prefixes.
 */
typedef void (*ei_log_handler)(struct ei *ei,
			       enum ei_log_priority priority,
			       const char *message,
			       bool is_continuation);
/**
 * Change the log handler for this context. If the log handler is NULL, the
 * built-in default log function is used.
 *
 * @param ei The EI context
 * @param log_handler The log handler or NULL to use the default log
 * handler.
 */
void
ei_log_set_handler(struct ei *ei, ei_log_handler log_handler);

void
ei_log_set_priority(struct ei *ei, enum ei_log_priority priority);

enum ei_log_priority
ei_log_get_priority(const struct ei *ei);

/**
 * Set the name for this client. This is a suggestion to the
 * server only and may not be honored.
 *
 * The client name may be used for display to the user, for example in
 * an authorization dialog that requires the user to approve a connection to
 * the EIS implementation.
 *
 * This function must be called immediately after ei_new() and before
 * setting up a backend with ei_setup_backend_socket() or
 * ei_setup_backend_fd().
 */
void
ei_configure_name(struct ei * ei, const char *name);

/**
 * Set this ei context to use the socket backend. The ei context will
 * connect to the socket at the given path and initiate the conversation
 * with the EIS server listening on that socket.
 *
 * If @p socketpath is `NULL`, the value of the environment variable
 * `LIBEI_SOCKET` is used. If @p socketpath does not start with '/', it is
 * relative to `$XDG_RUNTIME_DIR`. If `XDG_RUNTIME_DIR` is not set, this
 * function fails.
 *
 * If the connection was successful, an event of type @ref EI_EVENT_CONNECT
 * or @ref EI_EVENT_DISCONNECT will become available after a future call to
 * ei_dispatch().
 *
 * If the connection failed, use ei_unref() to release the data allocated
 * for this context.
 *
 * @return zero on success or a negative errno on failure
 */
int
ei_setup_backend_socket(struct ei *ei, const char *socketpath);

/**
 * Initialize the ei context on the given socket. The ei context will
 * initiate the conversation with the EIS server listening on the other end
 * of this socket.
 *
 * If the connection was successful, an event of type @ref EI_EVENT_CONNECT
 * or @ref EI_EVENT_DISCONNECT will become available after a future call to
 * ei_dispatch().
 *
 * If the connection failed, use ei_unref() to release the data allocated
 * for this context.
 *
 * @return zero on success or a negative errno on failure
 */
int
ei_setup_backend_fd(struct ei *ei, int fd);

/**
 * Connect to the `org.freedesktop.portal.Desktop` portal.
 *
 * @return 0 on success or a negative errno on failure
 */
int
ei_setup_backend_portal(struct ei *ei);

/**
 * Connect to an `org.freedesktop.portal.Desktop` implementation on the
 * given busname.
 *
 * Outside of testing environments, there is usually no reason to use
 * this function, use ei_setup_backend_portal() instead.
 *
 * @return 0 on success or a negative errno on failure
 */
int
ei_setup_backend_portal_busname(struct ei *ei, const char *busname);

/**
 * Increase the refcount of this struct by one. Use ei_unref() to decrease
 * the refcount.
 *
 * @return the argument passed into the function
 */
struct ei *
ei_ref(struct ei *ei);

/**
 * Decrease the refcount of this struct by one. When the refcount reaches
 * zero, the context disconnects from the server and all allocated resources
 * are released.
 *
 * @return always NULL
 */
struct ei *
ei_unref(struct ei *ei);

/**
 * Set a custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_get_user_data() to retrieve a previously set
 * user data.
 */
void
ei_set_user_data(struct ei *ei, void *user_data);

/**
 * Return the custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_set_user_data() to change the user data.
 */
void *
ei_get_user_data(struct ei *ei);

/**
 * libei keeps a single file descriptor for all events. This fd should be
 * monitored for events by the caller's mainloop, e.g. using select(). When
 * events are available on this fd, call libei_dispatch() immediately to
 * process.
 */
int
ei_get_fd(struct ei *ei);

/**
 * Main event dispatching function. Reads events of the file descriptors
 * and processes them internally. Use libei_get_event() to retrieve the
 * events.
 *
 * Dispatching does not necessarily queue events. This function
 * should be called immediately once data is available on the file
 * descriptor returned by libei_get_fd().
 */
void
ei_dispatch(struct ei *ei);

/**
 * Return the next event from the event queue, removing it from the queue.
 *
 * The returned object must be released by the caller with ei_event_unref()
 */
struct ei_event *
ei_get_event(struct ei *ei);

/**
 * Returns the next event in the internal event queue (or `NULL`) without
 * removing that event from the queue; the next call to ei_get_event()
 * will return that same event.
 *
 * This call is useful for checking whether there is an event and/or what
 * type of event it is.
 *
 * Repeated calls to ei_peek_event() return the same event.
 *
 * The returned event is refcounted, use ei_event_unref() to drop the
 * reference.
 *
 * A caller must not call ei_get_event() while holding a ref to an event
 * returned by ei_peek_event(). Doing so is undefined behavior.
 */
struct ei_event *
ei_peek_event(struct ei *ei);

/**
 * Release resources associated with this event. This function always
 * returns NULL.
 *
 * The caller cannot increase the refcount of an event. Events should be
 * considered transient data and not be held longer than required.
 * ei_event_unref() is provided for consistency (as opposed to, say,
 * ei_event_free()).
 */
struct ei_event *
ei_event_unref(struct ei_event *event);

const char *
ei_seat_get_name(struct ei_seat *seat);

bool
ei_seat_has_capability(struct ei_seat *seat,
		       enum ei_device_capability cap);

struct ei_seat *
ei_seat_ref(struct ei_seat *seat);

struct ei_seat *
ei_seat_unref(struct ei_seat *seat);

/**
 * Return the struct @ref ei context this seat is associated with.
 */
struct ei *
ei_seat_get_context(struct ei_seat *seat);

/**
 * @return the type of this event
 */
enum ei_event_type
ei_event_get_type(struct ei_event *event);

/**
 * Return the device from this event.
 *
 * For events of type @ref EI_EVENT_CONNECT and @ref EI_EVENT_DISCONNECT,
 * this function returns NULL.
 *
 * This does not increase the refcount of the device. Use eis_device_ref()
 * to keep a reference beyond the immediate scope.
 */
struct ei_device *
ei_event_get_device(struct ei_event *event);

/**
 * @return the event time in microseconds
 */
uint64_t
ei_event_get_time(struct ei_event *event);

/**
 * Increase the refcount of this struct by one. Use ei_device_unref() to
 * decrease the refcount.
 *
 * @return the argument passed into the function
 */
struct ei_device *
ei_device_ref(struct ei_device *device);

/**
 * Decrease the refcount of this struct by one. When the refcount reaches
 * zero, the context disconnects from the server and all allocated resources
 * are released.
 *
 * @return always NULL
 */
struct ei_device *
ei_device_unref(struct ei_device *device);

/**
 * Create a new device in the given seat. This device is a proxy
 * representing the server's device and should be used for initial device
 * configuration. It does not represent the server-created device until the
 * @ref EI_EVENT_DEVICE_ADDED for this device has been received.
 *
 * Use the configure API (e.g. ei_device_configure_capability()) to set up
 * the device, then call ei_device_add() to request creation of the device
 * by the server.
 *
 * @note A caller that does not want to add a created device to a seat
 *	**must** call ei_device_remove() for this device to ensure the
 *	resources are released.
 *
 * The returned object must be released by the caller with ei_event_unref()
 */
struct ei_device *
ei_device_new(struct ei_seat *ei_seat);

struct ei_seat *
ei_device_get_seat(struct ei_device *device);

/**
 * Set a custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_seat_get_user_data() to retrieve a
 * previously set user data.
 */
void
ei_seat_set_user_data(struct ei_seat *seat, void *user_data);

/**
 * Return the custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_seat_get_user_data() to change the user data.
 */
void *
ei_seat_get_user_data(struct ei_seat *seat);

/**
 * Set a custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_device_get_user_data() to retrieve a
 * previously set user data.
 */
void
ei_device_set_user_data(struct ei_device *device, void *user_data);

/**
 * Return the custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_device_get_user_data() to change the user data.
 */
void *
ei_device_get_user_data(struct ei_device *device);

/**
 * Set the name for this device. This is a suggestion only, the server may
 * ignore the name and assign a different one (or none). Use
 * ei_device_get_name() upon receiving @ref EI_EVENT_DEVICE_ADDED to get the
 * server-assigned
 * name.
 *
 * This function has no effect if called after ei_device_add()
 *
 * libei may quietly ignore names of unreasonable length. This is not
 * something a normal caller ever needs to worry about.
 *
 * @param device The EI device
 * @param name A name suggestion for the device
 */
void
ei_device_configure_name(struct ei_device *device, const char *name);

/**
 * Enable the given capability on this device. This is a suggestion only,
 * the server may ignore this capability.
 * Use ei_device_has_capability() upon receiving @ref EI_EVENT_DEVICE_ADDED
 * to check for the actual capabilities of the device.
 *
 * This function has no effect if called after ei_device_add()
 *
 * @param device The EI device
 * @param cap The capability to enable
 */
bool
ei_device_configure_capability(struct ei_device *device,
			       enum ei_device_capability cap);

/**
 * Set the range of the absolute pointer device in logical
 * pixels. The allowable range for absolute pointer motion is
 * [0, max) for each axis, i.e. zero inclusive, max exclusive. Coordinates
 * outside this range may be discarded or clipped silently by the library.
 *
 * The pointer range is constant. Where the pointer range is no longer
 * applicable, the client needs to remove the device and create and add a
 * new device with the updated pointer range.
 *
 * The server may use this in mapping heuristics. For example, a pointer
 * device with a pixel range of 1920x1200 **may** be automatically mapped by
 * the server to the monitor with this range, or a pointer device with a
 * ratio of R **may** be mapped to the monitor with the same ratio. This is
 * not a guarantee, the mapping policy is a private implementation detail
 * in the server. It is assumed that the client has other communication
 * channels (e.g. Wayland) to obtain the pointer range it needs to emulate
 * input on a device and channels to notify the server of desired mappings
 * (e.g. gsettings).
 *
 * It is a client bug to send pointer values outside this range.
 * It is a client bug to call this function on a device without the @ref
 * EI_DEVICE_CAP_POINTER_ABSOLUTE capability.
 *
 * This function has no effect if called after ei_device_add()
 *
 * @param device The EI device
 * @param width The maximum (exclusive) x value in logical pixels
 * @param height The maximum (exclusive) y value in logical pixels
 */
void
ei_device_pointer_configure_range(struct ei_device *device,
				  uint32_t width,
				  uint32_t height);

/**
 * Set the range of the touch device in logical pixels. This
 * function is identical to ei_device_pointer_configure_range() but
 * configures the touch range instead.
 */
void
ei_device_touch_configure_range(struct ei_device *device,
				uint32_t width,
				uint32_t height);


/**
 * Create a new keymap of the given @p type. This keymap does not immediately
 * apply to the device, use ei_device_keyboard_configure_keymap() to apply
 * this keymap.  A keymap may only be applied once and to a single device.
 *
 * The returned keymap has a refcount of at least 1, use ei_keymap_unref()
 * to release resources associated with this keymap.
 *
 * @param type The type of the keymap.
 * @param fd A memmap-able file descriptor of size @p size pointing to the
 * keymap used by this device. @p fd  can be closed by the caller after this
 * function completes.
 * @param size The size of the data at @p fd in bytes
 *
 * @return A keymap object or `NULL` on failure.
 */
struct ei_keymap *
ei_keymap_new(enum ei_keymap_type type, int fd, size_t size);

/**
 * @return the size of the keymap in bytes
 */
size_t
ei_keymap_get_size(struct ei_keymap *keymap);

/**
 * Returns the type for this keymap. The type specifies how to interpret the
 * data at the file descriptor returned by ei_keymap_get_fd().
 */
enum ei_keymap_type
ei_keymap_get_type(struct ei_keymap *keymap);

/**
 * Return a memmap-able file descriptor pointing to the keymap used by the
 * device. The keymap is constant for the lifetime of the device and
 * assigned to this device individually.
 */
int
ei_keymap_get_fd(struct ei_keymap *keymap);

/**
 * Returns the source for the keymap on this device, if any. This is a
 * convenience function for the client to check if its keymap was accepted.
 *
 * Where ei_device_keyboard_get_keymap() returns a value other `NULL` and
 * this function returns @ref EI_KEYMAP_SOURCE_CLIENT, the keymap is the one
 * provided with ei_device_keyboard_configure_keymap().
 *
 * Where ei_device_keyboard_get_keymap() returns a value other than `NULL`
 * and this function returns @ref EI_KEYMAP_SOURCE_SERVER, the keymap is one
 * created by the server and **not** the one provided with
 * ei_device_keyboard_configure_keymap().
 */
enum ei_keymap_source
ei_keymap_get_source(struct ei_keymap *keymap);

/**
 * Return the device this keymap belongs to, or `NULL` if it has not yet
 * been assigned to a device.
 *
 * Between ei_device_keyboard_configure_keymap() and libei processing an
 * @ref EI_EVENT_DEVICE_ADDED event, the returned device is the one provided
 * in ei_device_keyboard_configure_keymap().
 *
 * After processing and if the server changed the keymap or set the keymap
 * to NULL, this keymap may no longer be in use by the device and future
 * calls to this function return `NULL`.
 */
struct ei_device *
ei_keymap_get_device(struct ei_keymap *keymap);

/**
 * Increase the refcount of this struct by one. Use ei_keymap_unref() to
 * decrease the refcount.
 *
 * @return the argument passed into the function
 */
struct ei_keymap *
ei_keymap_ref(struct ei_keymap *keymap);

/**
 * Decrease the refcount of this struct by one. When the refcount reaches
 * zero, the context disconnects from the server and all allocated resources
 * are released.
 *
 * @return always NULL
 */
struct ei_keymap *
ei_keymap_unref(struct ei_keymap *keymap);

/**
 * Request this keymap for this device with the @ref EI_DEVICE_CAP_KEYBOARD
 * capability.
 *
 * The keymap for this device is a suggestion to the server, the actual
 * keymap used by this device is provided with the @ref
 * EI_EVENT_DEVICE_ADDED event. It is the client's responsibility to
 * handle the situation where the server assigns a specific keymap (or none)
 * that differs to the requested keymap.
 *
 * Note that keymap handling for individual input devices is largely
 * left to private implementation details in the server. For example,
 * modifier state or group handling may differ between server
 * implementations.
 *
 * A keymap can only be used once and for one device only.
 * Once a keymap has been assigned to a device, the caller may drop
 * remaining references using ei_keymap_unref().
 *
 * This function can only be called once per device, further calls are
 * ignored.
 *
 * This function has no effect if called after ei_device_add(). To change
 * the keymap, the device must be removed and re-added with a different
 * keymap.
 *
 * @param device The EI device
 * @param keymap A non-`NULL` keymap
 */
void
ei_device_keyboard_configure_keymap(struct ei_device *device,
				    struct ei_keymap *keymap);

/**
 * Request that the device be added to the server.
 * The server will respond with an @ref EI_EVENT_DEVICE_ADDED or @ref
 * EI_EVENT_DEVICE_REMOVED event once the request has been processed.
 *
 * A client can assume that an @ref EI_EVENT_DEVICE_REMOVED event is sent
 * for any device for which ei_device_add() was called before the @ref
 * EI_EVENT_DISCONNECT. Where a client gets
 * disconnected libei will emulate that event.
 *
 * A client may not send events through this device until it has been added
 * by the server.
 *
 * Devices should only be added once all events from ei_get_event() have
 * been processed. It is considered a client bug to add a device to a seat
 * after the SEAT_REMOVED has been received by libei.
 */
void
ei_device_add(struct ei_device *device);

/**
 * Notify the server that the device should be removed.
 *
 * Due to the asynchronous nature of the client-server interaction,
 * events for this device may still be in transit. The server will send an
 * @ref EI_EVENT_DEVICE_REMOVED event for this device. After that event,
 * device is considered removed by the server.
 *
 * A client can assume that an @ref EI_EVENT_DEVICE_REMOVED event is sent
 * for any device for which ei_device_remove() was called before the @ref
 * EI_EVENT_DISCONNECT. Where a client gets
 * disconnected libei will emulate that event.
 *
 * This does not release any resources associated with this device, use
 * ei_device_unref() for any references held by the client.
 */
void
ei_device_remove(struct ei_device *device);

/**
 * Return the name of this device.
 *
 * The return value of this function is constant after receiving the @ref
 * EI_EVENT_DEVICE_ADDED event. Before then, the returned string may be
 * freed at any time, callers must strdup() the returned value if they need
 * to keep the name around.
 *
 * @return the name of the device (if any) or NULL
 */
const char *
ei_device_get_name(struct ei_device *device);

/**
 * Return true if the device has the requested capability. Device
 * capabilities are constant after the @ref EI_EVENT_DEVICE_ADDED event.
 *
 * To change a device's capability, the device must be removed and a new
 * device with the different capabilities must be added.
 */
bool
ei_device_has_capability(struct ei_device *device,
			 enum ei_device_capability cap);

/**
 * Return the requested width for an @ref EI_DEVICE_CAP_POINTER_ABSOLUTE
 * device. The width and height is constant after the @ref
 * EI_EVENT_DEVICE_ADDED event.
 */
uint32_t
ei_device_pointer_get_width(struct ei_device *device);

/**
 * Return the requested height for an @ref EI_DEVICE_CAP_POINTER_ABSOLUTE
 * device. The width and height is constant after the @ref
 * EI_EVENT_DEVICE_ADDED event.
 */
uint32_t
ei_device_pointer_get_height(struct ei_device *device);

/**
 * Return the requested width for an @ref EI_DEVICE_CAP_TOUCH
 * device. The width and height is constant after the @ref
 * EI_EVENT_DEVICE_ADDED event.
 */
uint32_t
ei_device_touch_get_width(struct ei_device *device);

/**
 * Return the requested height for an @ref EI_DEVICE_CAP_TOUCH
 * device. The width and height is constant after the @ref
 * EI_EVENT_DEVICE_ADDED event.
 */
uint32_t
ei_device_touch_get_height(struct ei_device *device);

/**
 * Return the keymap for this device or `NULL`. The keymap is constant for
 * the lifetime of the device after the @ref EI_EVENT_DEVICE_ADDED was
 * received and applies to this device individually.
 *
 * If this function returns `NULL`, this device does not have
 * an individual keymap assigned. What keymap applies to the device in this
 * case is a server implementation detail.
 *
 * This does not increase the refcount of the keymap. Use ei_keymap_ref() to
 * keep a reference beyond the immediate scope.
 *
 * FIXME: the current API makes it impossible to know when the keymap has
 * been consumed so the file stays open forever.
 */
struct ei_keymap *
ei_device_keyboard_get_keymap(struct ei_device *device);

/**
 * Return the struct @ref ei_device this keymap is associated with.
 */
struct ei_device *
ei_keymap_get_context(struct ei_keymap *keymap);

/**
 * Return the struct @ref ei context this device is associated with.
 */
struct ei *
ei_device_get_context(struct ei_device *device);

/**
 * Generate a relative motion event on a device with
 * the @ref EI_DEVICE_CAP_POINTER capability.
 *
 * @param device The EI device
 * @param x The x movement in logical pixels
 * @param y The y movement in logical pixels
 */
void
ei_device_pointer_motion(struct ei_device *device, double x, double y);

/**
 * Generate an absolute motion event on a device with
 * the @ref EI_DEVICE_CAP_POINTER_ABSOLUTE capability.
 *
 * The required conditions are:
 * - 0 <= x < width
 * - 0 <= y < height
 * If these conditions are not met, the event is silently discarded.
 *
 * @param device The EI device
 * @param x The x position in logical pixels
 * @param y The y position in logical pixels
 */
void
ei_device_pointer_motion_absolute(struct ei_device *device,
				  double x, double y);

/**
 * Generate a button event on a device with
 * the @ref EI_DEVICE_CAP_POINTER_ABSOLUTE or
 * @ref EI_DEVICE_CAP_POINTER capability.
 *
 * Button codes must match the defines in linux/input-event-codes.h
 *
 * @param device The EI device
 * @param button The button code
 * @param is_press true for button press, false for button release
 */
void
ei_device_pointer_button(struct ei_device *device,
			 uint32_t button, bool is_press);

/**
 * Generate a scroll event on a device with
 * the @ref EI_DEVICE_CAP_POINTER_ABSOLUTE or
 * @ref EI_DEVICE_CAP_POINTER capability.
 *
 * @note The server is responsible for emulateing discrete scrolling based
 * on the pixel value, do not call ei_device_pointer_scroll_discrete() for
 * the same input event.
 *
 * @param device The EI device
 * @param x The x scroll distance in logical pixels
 * @param y The y scroll distance in logical pixels
 *
 * @see ei_device_pointer_scroll_discrete
 */
void
ei_device_pointer_scroll(struct ei_device *device, double x, double y);

/**
 * Generate a discrete scroll event on a device with
 * the @ref EI_DEVICE_CAP_POINTER_ABSOLUTE or
 * @ref EI_DEVICE_CAP_POINTER capability.
 *
 * A discrete scroll event is based logical scroll units (equivalent to one
 * mouse wheel click). The value for one scroll unit is 120, a fraction or
 * multiple thereof represents a fraction or multiple of a wheel click.
 *
 * @note The server is responsible for emulating pixel-based scrolling based
 * on the discrete value, do not call ei_device_pointer_scroll() for the
 * same input event.
 *
 * @param device The EI device
 * @param x The x scroll distance in fractions or multiples of 120
 * @param y The y scroll distance in fractions or multiples of 120
 *
 * @see ei_device_pointer_scroll
 */
void
ei_device_pointer_scroll_discrete(struct ei_device *device, int32_t x, int32_t y);

/**
 * Generate a key event on a device with
 * the @ref EI_DEVICE_CAP_KEYBOARD capability.
 *
 * Keys use the evdev scan codes as defined in
 * linux/input-event-codes.h
 *
 * @param device The EI device
 * @param keycode The key code
 * @param is_press true for key down, false for key up
 */
void
ei_device_keyboard_key(struct ei_device *device, uint32_t keycode, bool is_press);

/**
 * Initiate a new touch on a device with the @ref EI_DEVICE_CAP_TOUCH
 * capability. This touch does not immediately send events, use
 * ei_touch_down(), ei_touch_motion(), and ei_touch_up().
 *
 * The returned touch has a refcount of at least 1, use ei_touch_unref() to
 * release resources associated with this touch
 */
struct ei_touch *
ei_device_touch_new(struct ei_device *device);

/**
 * This function can only be called once on an ei_touch object. Further
 * calls to ei_touch_down() on the same object are silently ignored.
 *
 * The required conditions are:
 * - 0 <= x < width
 * - 0 <= y < height
 *
 * @param touch A newly created touch
 * @param x The x position in logical pixels
 * @param y The y position in logical pixels
 */
void
ei_touch_down(struct ei_touch *touch, double x, double y);

/**
 * Move this touch to the new coordinates.
 */
void
ei_touch_motion(struct ei_touch *touch, double x, double y);

/**
 * Release this touch. After this call, the touch event becomes inert and
 * no longer responds to either ei_touch_down(), ei_touch_motion() or
 * ei_touch_up() and the caller should call ei_touch_unref().
 */
void
ei_touch_up(struct ei_touch *touch);

/**
 * Increase the refcount of this struct by one. Use ei_touch_unref() to
 * decrease the refcount.
 *
 * @return the argument passed into the function
 */
struct ei_touch *
ei_touch_ref(struct ei_touch *touch);

/**
 * Decrease the refcount of this struct by one. When the refcount reaches
 * zero, the context disconnects from the server and all allocated resources
 * are released.
 *
 * @return always NULL
 */
struct ei_touch *
ei_touch_unref(struct ei_touch *touch);

/**
 * Return the custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_touch_set_user_data() to change the user data.
 */
void
ei_touch_set_user_data(struct ei_touch *touch, void *user_data);

/**
 * Set a custom data pointer for this context. libei will not look at or
 * modify the pointer. Use ei_touch_get_user_data() to retrieve a previously
 * set user data.
 */
void *
ei_touch_get_user_data(struct ei_touch *touch);

/**
 * @return the device this touch originates on
 */
struct ei_device *
ei_touch_get_device(struct ei_touch *touch);

/**
 * Return the seat from this event.
 *
 * For events of type @ref EI_EVENT_CONNECT and @ref EI_EVENT_DISCONNECT,
 * this function returns NULL.
 *
 * This does not increase the refcount of the seat. Use eis_seat_ref()
 * to keep a reference beyond the immediate scope.
 */
struct ei_seat *
ei_event_get_seat(struct ei_event *event);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif
