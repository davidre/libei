/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <libeis.h>

/**
 * REIS is the library for Restrictions for EIS. This library is used by
 * intermediaries between EI and EIS to reduce the capabilities that an EI
 * client has available.
 *
 * It is a helper library that does not initiate a full EI or EIS
 * context but works on the file descriptor instead.
 *
 * Restricting capabilities is a one-way-road. A default EIS context has
 * full permissions, consecutive calls can only restrict the current
 * permissions but not loosen them.
 */

struct reis;

struct reis *
reis_new(void);

struct reis *
reis_unref(struct reis* reis);

/**
 * Apply the currently configured set of restrictions to the EIS context at
 * the other end of eisfd. The EIS context does not need to be in any
 * specific state and no checking is done that there is indeed an EIS
 * context at the other end of the fd.
 *
 * Once applied, the reis context should be released with reis_unref()
 *
 * @return zero on success or a negative errno otherwise
 */
int
reis_apply(struct reis *reis, int eisfd);

/**
 * Set the name for the client on this connection.
 *
 * This function has no effect if the EI client has already sent the
 * connection message to the server. IOW this can only be used to *set* the
 * name but not to *change* the name of the client.
 *
 * Calling this function multiple times has no effect, only the first name
 * is used.
 *
 * @return zero on success or a negative errno otherwise
 */
int
reis_set_name(struct reis *reis, const char *name);

/**
 * Change the default policy for capabilities to "allow". Capabilities are
 * allowed unless explicitly denied by reis_deny_cap().
 *
 * This function has no effect if the default policy is "deny".
 */
int
reis_set_cap_policy_allow(struct reis *reis);

/**
 * Change the default policy for capabilities to "deny". Capabilities are
 * denied unless explicitly denied by reis_allow_cap().
 *
 * A caller must call reis_allow_cap() *before* calling
 * reis_set_cap_policy_deny(), once the default deny policy is in place, the
 * allowed capabilities cannot be expanded further.
 *
 * A capability is permitted if:
 * - the policy is allow and the capability is not in the deny list
 * - the policy is deny and the capability was added to the allow list
 *   before the policy was set to deny
 */
int
reis_set_cap_policy_deny(struct reis *reis);

/**
 * Explicitly allow the given capability.
 *
 * This function may be called multiple times before reis_apply() to create
 * a set of allowed capabilities.
 *
 * Where reis_apply() is called multiple times with reis_allow_cap(), the
 * final set of allowed capabilities is the intersection of all sets.
 * In other words, calling reis_apply() once with pointer and keyboard caps
 * and once with just keyboard caps results in just keyboard caps.
 */
int
reis_allow_cap(struct reis *reis, enum eis_device_capability cap);

/**
 * Explicitly deny the given capability.
 *
 * This function may be called multiple times before reis_apply() to create
 * a set of denied capabilities.
 *
 * Where reis_apply() is called multiple times with reis_deny_cap(), the
 * final set of denied capabilities is the union of all sets.
 * In other words, calling reis_apply() once with pointer caps denied
 * and once with keyboard caps denied results in both pointer and keyboard
 * caps denied.
 */
int
reis_deny_cap(struct reis *reis, enum eis_device_capability cap);
