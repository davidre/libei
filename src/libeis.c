/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "util-macros.h"
#include "util-object.h"
#include "util-sources.h"
#include "util-strings.h"

#include "libeis.h"
#include "libeis-private.h"

static void
eis_destroy(struct eis *eis)
{
	struct eis_client *c, *tmp;

	list_for_each_safe(c, tmp, &eis->clients, link) {
		eis_client_disconnect(c);
	}

	struct eis_event *e;
	while ((e = eis_get_event(eis)) != NULL)
		eis_event_unref(e);

	if (eis->backend_interface.destroy)
		eis->backend_interface.destroy(eis, eis->backend);
	sink_unref(eis->sink);
}

static
OBJECT_IMPLEMENT_CREATE(eis);
_public_
OBJECT_IMPLEMENT_REF(eis);
_public_
OBJECT_IMPLEMENT_UNREF(eis);
_public_
OBJECT_IMPLEMENT_SETTER(eis, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(eis, user_data, void *);

_public_ struct eis *
eis_new(void *user_data)
{
	_unref_(eis) *eis = eis_create(NULL);

	list_init(&eis->clients);
	list_init(&eis->event_queue);

	eis_log_set_handler(eis, NULL);
	eis_log_set_priority(eis, EIS_LOG_PRIORITY_INFO);
	eis->sink = sink_new();
	if (!eis->sink)
		return NULL;

	eis->user_data = user_data;

	return steal(&eis);
}

_public_ int
eis_get_fd(struct eis *eis)
{
	return sink_get_fd(eis->sink);
}

_public_ void
eis_dispatch(struct eis *eis)
{
	sink_dispatch(eis->sink);
}

static const char *
eis_event_type_to_string(enum eis_event_type type)
{
	switch(type) {
	CASE_RETURN_STRING(EIS_EVENT_CLIENT_CONNECT);
	CASE_RETURN_STRING(EIS_EVENT_CLIENT_DISCONNECT);
	CASE_RETURN_STRING(EIS_EVENT_DEVICE_ADDED);
	CASE_RETURN_STRING(EIS_EVENT_DEVICE_REMOVED);
	CASE_RETURN_STRING(EIS_EVENT_POINTER_MOTION);
	CASE_RETURN_STRING(EIS_EVENT_POINTER_MOTION_ABSOLUTE);
	CASE_RETURN_STRING(EIS_EVENT_POINTER_BUTTON);
	CASE_RETURN_STRING(EIS_EVENT_POINTER_SCROLL);
	CASE_RETURN_STRING(EIS_EVENT_POINTER_SCROLL_DISCRETE);
	CASE_RETURN_STRING(EIS_EVENT_KEYBOARD_KEY);
	CASE_RETURN_STRING(EIS_EVENT_TOUCH_DOWN);
	CASE_RETURN_STRING(EIS_EVENT_TOUCH_UP);
	CASE_RETURN_STRING(EIS_EVENT_TOUCH_MOTION);
	}

	assert(!"Unhandled event type");
}

static void
eis_queue_event(struct eis_event *event)
{
	struct eis *eis = eis_event_get_context(event);

	log_debug(eis, "queuing event type %s (%d)\n",
		  eis_event_type_to_string(event->type), event->type);

	list_append(&eis->event_queue, &event->link);
}

void
eis_queue_connect_event(struct eis_client *client)
{
	struct eis_event *e = eis_event_new_for_client(client);
	e->type = EIS_EVENT_CLIENT_CONNECT;
	eis_queue_event(e);
}

void
eis_queue_disconnect_event(struct eis_client *client)
{
	struct eis_event *e = eis_event_new_for_client(client);
	e->type = EIS_EVENT_CLIENT_DISCONNECT;
	eis_queue_event(e);
}

void
eis_queue_added_event(struct eis_device *device)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_DEVICE_ADDED;
	eis_queue_event(e);
}

void
eis_queue_removed_event(struct eis_device *device)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_DEVICE_REMOVED;
	eis_queue_event(e);
}

void
eis_queue_pointer_rel_event(struct eis_device *device,
			    double dx, double dy)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_POINTER_MOTION;
	e->pointer.dx = dx;
	e->pointer.dy = dy;
	eis_queue_event(e);
}

void
eis_queue_pointer_abs_event(struct eis_device *device,
			    double x, double y)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_POINTER_MOTION_ABSOLUTE;
	e->pointer.absx = x;
	e->pointer.absy = y;
	eis_queue_event(e);
}

void
eis_queue_pointer_button_event(struct eis_device *device, uint32_t button,
			       bool is_press)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_POINTER_BUTTON;
	e->pointer.button = button;
	e->pointer.button_is_press = is_press;

	eis_queue_event(e);
}

void
eis_queue_keyboard_key_event(struct eis_device *device, uint32_t key,
			     bool is_press)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_KEYBOARD_KEY;
	e->keyboard.key = key;
	e->keyboard.key_is_press = is_press;
	eis_queue_event(e);
}

void
eis_queue_touch_down_event(struct eis_device *device, uint32_t touchid,
			   double x, double y)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_TOUCH_DOWN;
	e->touch.touchid = touchid,
	e->touch.x = x;
	e->touch.y = y;
	eis_queue_event(e);
}

void
eis_queue_touch_motion_event(struct eis_device *device, uint32_t touchid,
			     double x, double y)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_TOUCH_MOTION;
	e->touch.touchid = touchid,
	e->touch.x = x;
	e->touch.y = y;
	eis_queue_event(e);
}

void
eis_queue_touch_up_event(struct eis_device *device, uint32_t touchid)
{
	struct eis_event *e = eis_event_new_for_device(device);
	e->type = EIS_EVENT_TOUCH_UP;
	e->touch.touchid = touchid,
	eis_queue_event(e);
}

_public_ struct eis_event*
eis_get_event(struct eis *eis)
{
	if (list_empty(&eis->event_queue))
		return NULL;

	struct eis_event *e = list_first_entry(&eis->event_queue, e, link);
	list_remove(&e->link);

	return e;
}

_public_ struct eis_event *
eis_peek_event(struct eis *eis)
{
	if (list_empty(&eis->event_queue))
		return NULL;

	struct eis_event *e = list_first_entry(&eis->event_queue, e, link);
	return eis_event_ref(e);
}

void
eis_add_client(struct eis *eis, struct eis_client *client)
{
	list_append(&eis->clients, &client->link);
}
