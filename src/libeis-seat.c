/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include "util-macros.h"
#include "util-bits.h"
#include "util-strings.h"
#include "libeis-private.h"

static void
eis_seat_destroy(struct eis_seat *seat)
{
	struct eis_device *d;

	/* We expect those to have been removed already*/
	list_for_each(d, &seat->devices, link) {
		assert(!"device list not empty");
	}
	free(seat->name);
}

_public_
OBJECT_IMPLEMENT_REF(eis_seat);
_public_
OBJECT_IMPLEMENT_UNREF(eis_seat);
static
OBJECT_IMPLEMENT_CREATE(eis_seat);
static
OBJECT_IMPLEMENT_PARENT(eis_seat, eis_client);
_public_
OBJECT_IMPLEMENT_GETTER(eis_seat, user_data, void *);
_public_
OBJECT_IMPLEMENT_SETTER(eis_seat, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(eis_seat, name, const char *);

_public_ struct eis_client *
eis_seat_get_client(struct eis_seat *seat)
{
	return eis_seat_parent(seat);
}

_public_ struct eis_seat *
eis_client_new_seat(struct eis_client *client, const char *name)
{
	static uint32_t seatid = 0x10000; /* we leave the lower bits to the deviceids */
	struct eis_seat *seat = eis_seat_create(&client->object);

	seat->id = seatid++;
	seat->state = EIS_SEAT_STATE_PENDING;
	seat->name = xstrdup(name);
	list_init(&seat->devices);
	/* seat is owned by caller until it's added */
	list_append(&client->seats_pending, &seat->link);

	return seat;
}

_public_ void
eis_seat_add(struct eis_seat *seat)
{
	struct eis_client *client = eis_seat_get_client(seat);

	switch (seat->state) {
	case EIS_SEAT_STATE_PENDING:
		break;
	case EIS_SEAT_STATE_ADDED:
	case EIS_SEAT_STATE_REMOVED:
	case EIS_SEAT_STATE_DEAD:
		log_bug_client(eis_client_get_context(client),
			       "%s: seat already added/removed/dead\n", __func__);
		return;
	}

	seat->state = EIS_SEAT_STATE_ADDED;
	eis_client_add_seat(client, seat);
}

_public_ void
eis_seat_remove(struct eis_seat *seat)
{
	struct eis_client *client = eis_seat_get_client(seat);

	switch (seat->state) {
	case EIS_SEAT_STATE_PENDING:
	case EIS_SEAT_STATE_ADDED:
		break;
	case EIS_SEAT_STATE_REMOVED:
	case EIS_SEAT_STATE_DEAD:
		log_bug_client(eis_client_get_context(client),
			       "%s: seat already removed\n", __func__);
		return;
	}

	/* We have a problem here: the server controls the seat but
	 * we wait for the client to acknowledge device removal. so we can't
	 * actually remove the seat until all devices within are gone.
	 *
	 * So we disconnect the device (sending a message to the client)
	 * and mark the seat as removed. When the messages for the
	 * removed devices come in from the client, we can remove the seat
	 * for real.
	 */
	struct eis_device *d, *tmp;
	list_for_each_safe(d, tmp, &seat->devices, link) {
		eis_device_disconnect(d);
	}

	eis_client_remove_seat(eis_seat_get_client(seat), seat);
	seat->state = EIS_SEAT_STATE_REMOVED;
}

void
eis_seat_disconnect(struct eis_seat *seat)
{
	/* First, remove the seat as if the caller did it */
	eis_seat_remove(seat);

	/* Now process all devices as if the client had sent us the
	 * device removed event. */
	struct eis_device *d, *tmp;
	list_for_each_safe(d, tmp, &seat->devices, link) {
		eis_device_removed_by_client(d);
	}

	seat->state = EIS_SEAT_STATE_DEAD;
	list_remove(&seat->link);
	eis_seat_unref(seat);
}

_public_ void
eis_seat_allow_capability(struct eis_seat *seat,
			  enum eis_device_capability cap)
{
	switch (cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		seat->capabilities_mask |= bit(cap);
		break;
	}
}
