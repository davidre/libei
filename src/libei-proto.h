/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "config.h"

#include "util-macros.h"
#include "util-mem.h"

#include "brei-shared.h"
#include "libei-private.h"

/* The message type for the wire format */
enum message_type {
	MESSAGE_CONNECTED = 1,
	MESSAGE_DISCONNECTED,
	MESSAGE_SEAT_ADDED,
	MESSAGE_SEAT_REMOVED,
	MESSAGE_DEVICE_ADDED,
	MESSAGE_DEVICE_REMOVED,
	MESSAGE_DEVICE_RESUMED,
	MESSAGE_DEVICE_SUSPENDED,
};

static inline const char *
message_type_to_string(enum message_type type)
{
	switch(type) {
	CASE_RETURN_STRING(MESSAGE_CONNECTED);
	CASE_RETURN_STRING(MESSAGE_DISCONNECTED);
	CASE_RETURN_STRING(MESSAGE_SEAT_ADDED);
	CASE_RETURN_STRING(MESSAGE_SEAT_REMOVED);
	CASE_RETURN_STRING(MESSAGE_DEVICE_ADDED);
	CASE_RETURN_STRING(MESSAGE_DEVICE_REMOVED);
	CASE_RETURN_STRING(MESSAGE_DEVICE_RESUMED);
	CASE_RETURN_STRING(MESSAGE_DEVICE_SUSPENDED);
	}
	assert(!"Unimplemented message type");
}

struct message {
	enum message_type type;
	union {
		struct message_connected {
			uint8_t pad; /* no data */
		} connected;
		struct message_disconnected {
			uint8_t pad; /* no data */
		} disconnected;
		struct message_seat_added {
			uint32_t seatid;
			char *name;
			uint32_t capabilities;
		} seat_added;
		struct message_seat_removed {
			uint32_t seatid;
		} seat_removed;
		struct message_device_added {
			uint32_t deviceid;
			char *name;
			uint32_t capabilities;
			enum ei_keymap_type keymap_type;
			bool keymap_from_server;
			int keymap_fd;
			size_t keymap_size;
			uint32_t seatid;
		} device_added;
		struct message_device_removed {
			uint32_t deviceid;
		} device_removed;
		struct message_device_resumed {
			uint32_t deviceid;
		} resumed;
		struct message_device_suspended {
			uint32_t deviceid;
		} suspended;
	};
};

void message_free(struct message *msg);

DEFINE_TRIVIAL_CLEANUP_FUNC(struct message*, message_free);
#define _cleanup_message_ _cleanup_(message_freep)

struct message *
ei_proto_parse_message(struct brei_message *message, size_t *consumed);

int
ei_proto_send_connect(struct ei *ei);

int
ei_proto_send_disconnect(struct ei *ei);

int
ei_proto_send_add(struct ei *ei, struct ei_device *device);

int
ei_proto_send_remove(struct ei *ei, struct ei_device *device);

int
ei_proto_send_rel(struct ei *ei, struct ei_device *device,
		    double x, double y);

int
ei_proto_send_abs(struct ei *ei, struct ei_device *device,
		  double x, double y);

int
ei_proto_send_button(struct ei *ei, struct ei_device *device,
		       uint32_t b, bool is_press);

int
ei_proto_send_key(struct ei *ei, struct ei_device *device,
		    uint32_t k, bool is_press);

int
ei_proto_send_touch_down(struct ei *ei, struct ei_device *device,
			 uint32_t tid, double x, double y);

int
ei_proto_send_touch_motion(struct ei *ei, struct ei_device *device,
			   uint32_t tid, double x, double y);

int
ei_proto_send_touch_up(struct ei *ei, struct ei_device *device, uint32_t tid);
