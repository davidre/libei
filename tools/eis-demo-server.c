/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <poll.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <sys/mman.h>

#if HAVE_LIBXKBCOMMON
#include <xkbcommon/xkbcommon.h>
#endif

#include "src/util-color.h"
#include "src/util-mem.h"
#include "src/util-memfile.h"
#include "src/util-strings.h"

#include "eis-demo-server.h"

static bool stop = false;

static void sighandler(int signal) {
	stop = true;
}

DEFINE_UNREF_CLEANUP_FUNC(eis);
DEFINE_UNREF_CLEANUP_FUNC(eis_event);
DEFINE_UNREF_CLEANUP_FUNC(eis_keymap);
DEFINE_UNREF_CLEANUP_FUNC(eis_seat);

static void unlink_free(char **path) {
	if (*path) {
		unlink(*path);
		free(*path);
	}
}
#define _cleanup_unlink_free_ _cleanup_(unlink_free)

static inline void
_printf_(1, 2)
colorprint(const char *format, ...)
{
	static uint64_t color = 0;
	run_only_once {
		color = rgb(1, 1, 1) | rgb_bg(255, 127, 0);
	}

	cprintf(color, "EIS socket server: ");
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}

#if HAVE_LIBXKBCOMMON
DEFINE_UNREF_CLEANUP_FUNC(xkb_context);
DEFINE_UNREF_CLEANUP_FUNC(xkb_keymap);
DEFINE_UNREF_CLEANUP_FUNC(xkb_state);
#endif

static void
setup_server_keymap(struct eis_demo_server *server, struct eis_device *device)
{
#if HAVE_LIBXKBCOMMON
	colorprint("Using server layout: %s\n", server->layout);
	_unref_(xkb_context) *ctx = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!ctx)
		return;

	struct xkb_rule_names names = {
		.rules = "evdev",
		.model = "pc105",
		.layout = server->layout,
	};

	_unref_(xkb_keymap) *keymap = xkb_keymap_new_from_names(ctx, &names, 0);
	if (!keymap)
		return;

	const char *str = xkb_keymap_get_as_string(keymap, XKB_KEYMAP_FORMAT_TEXT_V1);
	size_t len = strlen(str) - 1;

	struct memfile *f = memfile_new(str, len);
	if (!f)
		return;

	_unref_(eis_keymap) *k = eis_keymap_new(EIS_KEYMAP_TYPE_XKB, memfile_get_fd(f),
						memfile_get_size(f));
	eis_device_keyboard_set_keymap(device, k);
	memfile_unref(f);

	_unref_(xkb_state) *state = xkb_state_new(keymap);
	if (!state)
		return;

	server->ctx = steal(&ctx);
	server->keymap = steal(&keymap);
	server->state = steal(&state);
#endif
}

static void
setup_client_keymap(struct eis_demo_server *server, struct eis_device *device)
{
#if HAVE_LIBXKBCOMMON
	struct eis_keymap *keymap = eis_device_keyboard_get_keymap(device);
	if (!keymap)
		return;

	colorprint("Using client keymap\n");
	int fd = eis_keymap_get_fd(keymap);
	size_t size = eis_keymap_get_size(keymap);
	char *str = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (str == MAP_FAILED)
		return;

	_unref_(xkb_context) *ctx = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!ctx)
		return;

	_unref_(xkb_keymap) *xkb_keymap = xkb_keymap_new_from_string(ctx, str,
								    XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	if (!keymap)
		return;

	_unref_(xkb_state) *state = xkb_state_new(xkb_keymap);
	if (!state)
		return;

	server->ctx = steal(&ctx);
	server->keymap = steal(&xkb_keymap);
	server->state = steal(&state);
#endif
}

static void
setup_keymap(struct eis_demo_server *server, struct eis_device *device)
{
	if (server->layout)
		setup_server_keymap(server, device);
	else
		setup_client_keymap(server, device);
}

static void
handle_key(struct eis_demo_server *server, uint32_t keycode, bool is_press)
{
	char keysym_name[64] = {0};

#if HAVE_LIBXKBCOMMON
	if (server->state) {
		uint32_t xkbkc = keycode + 8;
		xkb_state_update_key(server->state, xkbkc, is_press ? XKB_KEY_DOWN : XKB_KEY_UP);
		xkb_state_key_get_utf8(server->state, xkbkc, keysym_name, sizeof(keysym_name));
	}
#endif
	colorprint("key %d (%s) [%s]\n",
		   keycode, is_press ? "press" : "release",
		   keysym_name);
}

/**
 * The simplest event handler. Connect any client and any device and just
 * printf the events as the come in. This is an incomplete implementation,
 * it just does the basics for pointers and keyboards atm.
 */
static int
eis_demo_server_printf_handle_event(struct eis_demo_server *server,
				    struct eis_event *e)
{
	switch(eis_event_get_type(e)) {
	case EIS_EVENT_CLIENT_CONNECT:
		{
		struct eis_client *client = eis_event_get_client(e);
		colorprint("new client: %s\n", eis_client_get_name(client));
		/* insert sophisticated authentication here */
		eis_client_connect(client);
		colorprint("accepting client, creating new seat 'default'\n");
		_unref_(eis_seat) *seat = eis_client_new_seat(client, "default");
		eis_seat_allow_capability(seat, EIS_DEVICE_CAP_POINTER);
		eis_seat_allow_capability(seat, EIS_DEVICE_CAP_POINTER_ABSOLUTE);
		eis_seat_allow_capability(seat, EIS_DEVICE_CAP_KEYBOARD);
		eis_seat_allow_capability(seat, EIS_DEVICE_CAP_TOUCH);
		eis_seat_add(seat);
		break;
		}
	case EIS_EVENT_CLIENT_DISCONNECT:
		{
		struct eis_client *client = eis_event_get_client(e);
		colorprint("client %s disconnected\n", eis_client_get_name(client));
		eis_client_disconnect(client);
		break;
		}
	case EIS_EVENT_DEVICE_ADDED:
		{
		struct eis_device *device = eis_event_get_device(e);
		colorprint("new device, caps:%s%s%s%s\n",
		       eis_device_has_capability(device, EIS_DEVICE_CAP_POINTER) ? " ptr" : "",
		       eis_device_has_capability(device, EIS_DEVICE_CAP_KEYBOARD) ? " kbd" : "",
		       eis_device_has_capability(device, EIS_DEVICE_CAP_POINTER_ABSOLUTE) ? " abs" : "",
		       eis_device_has_capability(device, EIS_DEVICE_CAP_TOUCH) ? " touch" : "");

		eis_device_allow_capability(device, EIS_DEVICE_CAP_POINTER);
		eis_device_allow_capability(device, EIS_DEVICE_CAP_POINTER_ABSOLUTE);
		eis_device_allow_capability(device, EIS_DEVICE_CAP_KEYBOARD);
		eis_device_allow_capability(device, EIS_DEVICE_CAP_TOUCH);

		setup_keymap(server, device);
		/* insert sophisticated device checks here */
		eis_device_connect(device);
		eis_device_resume(device);
		break;
		}
	case EIS_EVENT_DEVICE_REMOVED:
		{
		colorprint("device removed\n");
		break;
		}
	case EIS_EVENT_POINTER_MOTION:
		{
		colorprint("motion by %.2f/%.2f\n",
		       eis_event_pointer_get_dx(e),
		       eis_event_pointer_get_dy(e));
		}
		break;
	case EIS_EVENT_POINTER_MOTION_ABSOLUTE:
		{
		colorprint("absmotion to %.2f/%.2f\n",
		       eis_event_pointer_get_absolute_x(e),
		       eis_event_pointer_get_absolute_y(e));
		}
		break;
	case EIS_EVENT_POINTER_BUTTON:
		{
		colorprint("button %d (%s)\n",
		       eis_event_pointer_get_button(e),
		       eis_event_pointer_get_button_is_press(e) ? "press" : "release");
		}
		break;
	case EIS_EVENT_KEYBOARD_KEY:
		{
		handle_key(server,
			   eis_event_keyboard_get_key(e),
			   eis_event_keyboard_get_key_is_press(e));
		}
		break;
	default:
		abort();
	}
	return 0;
}

static void
usage(FILE *fp, const char *argv0)
{
	fprintf(fp,
		"Usage: %s [--verbose] [--uinput] [--socketpath=/path/to/socket] [--force]\n"
		"\n"
		"Start an EIS demo server. The server accepts all client connections\n"
		"and devices and prints any events from the client to stdout.\n"
		"\n"
		"Options:\n"
		" --socketpath	Use the given socket path. Default: $XDG_RUNTIME/eis-0\n"
		" --force	Remove the socket if it already exists\n"
		" --layout	Use the given XKB layout (requires libxkbcommon). Default:\n"
		"		use the client-supplied keymap, if any\n"
		" --uinput	Set up each device as uinput device (this requires root)\n"
		" --verbose	Enable debugging output\n"
		"",
		argv0);
}

int main(int argc, char **argv)
{
	bool verbose = false;
	bool uinput = false;
	bool force = false;
	const char *layout = NULL;

	_cleanup_unlink_free_ char *socketpath = NULL;
	const char *xdg = getenv("XDG_RUNTIME_DIR");
	if (xdg)
		socketpath = xaprintf("%s/eis-0", xdg);

	while (true) {
		enum {
			OPT_VERBOSE,
			OPT_LAYOUT,
			OPT_SOCKETPATH,
			OPT_FORCE,
			OPT_UINPUT,
		};
		static struct option long_opts[] = {
			{"socketpath",	required_argument, 0, OPT_SOCKETPATH},
			{"layout",	required_argument, 0, OPT_LAYOUT},
			{"uinput",	no_argument, 0, OPT_UINPUT},
			{"force",	no_argument, 0, OPT_FORCE},
			{"verbose",	no_argument,0, OPT_VERBOSE},
			{"help",	no_argument,0, 'h'},
			{NULL},
		};

		int optind = 0;
		int c = getopt_long(argc, argv, "h", long_opts, &optind);
		if (c == -1)
			break;

		switch(c) {
			case 'h':
				usage(stdout, argv[0]);
				return EXIT_SUCCESS;
			case OPT_SOCKETPATH:
				free(socketpath);
				socketpath = xstrdup(optarg);
				break;
			case OPT_LAYOUT:
				layout = optarg;
				break;
			case OPT_UINPUT:
				uinput = true;
				break;
			case OPT_FORCE:
				force = true;
				break;
			case OPT_VERBOSE:
				verbose = true;
				break;
			default:
				usage(stderr, argv[0]);
				return EXIT_FAILURE;
		}
	}

	if (socketpath == NULL) {
		fprintf(stderr, "No socketpath given and $XDG_RUNTIME_DIR is not set\n");
		return EXIT_FAILURE;
	}


	struct eis_demo_server server = {
		.layout = layout,
		.handler.handle_event = eis_demo_server_printf_handle_event,
	};

	if (uinput) {
		int rc = -ENOTSUP;
#if HAVE_LIBEVDEV
		rc = eis_demo_server_setup_uinput_handler(&server);
#endif
		if (rc != 0) {
			fprintf(stderr, "Failed to set up uinput handler: %s\n", strerror(-rc));
			return EXIT_FAILURE;
		}
	}

	_unref_(eis) *eis = eis_new(NULL);
	assert(eis);

	if (verbose)
		eis_log_set_priority(eis, EIS_LOG_PRIORITY_DEBUG);

	signal(SIGINT, sighandler);

	if (force)
		unlink(socketpath);

	int rc = eis_setup_backend_socket(eis, socketpath);
	if (rc != 0) {
		fprintf(stderr, "init failed: %s\n", strerror(errno));
		return 1;
	}

	colorprint("waiting on %s\n", socketpath);

	struct pollfd fds = {
		.fd = eis_get_fd(eis),
		.events = POLLIN,
		.revents = 0,
	};

	while (!stop && poll(&fds, 1, -1) > -1) {
		eis_dispatch(eis);

		while (true) {
			_unref_(eis_event) *e = eis_get_event(eis);
			if (!e)
				break;

			int rc = server.handler.handle_event(&server, e);
			if (rc != 0)
				break;
		}
	}

	return 0;
}
