/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <poll.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdarg.h>

#if HAVE_LIBXKBCOMMON
#include <xkbcommon/xkbcommon.h>
#endif

#include "libei.h"

#include "src/util-macros.h"
#include "src/util-mem.h"
#include "src/util-memfile.h"
#include "src/util-color.h"
#include "src/util-strings.h"

DEFINE_UNREF_CLEANUP_FUNC(ei);
DEFINE_UNREF_CLEANUP_FUNC(ei_device);
DEFINE_UNREF_CLEANUP_FUNC(ei_keymap);
DEFINE_UNREF_CLEANUP_FUNC(ei_event);

static inline void
_printf_(1, 2)
colorprint(const char *format, ...)
{
	static uint64_t color = 0;
	run_only_once {
		color = rgb(1, 1, 1) | rgb_bg(230, 0, 230);
	}

	cprintf(color, "EI socket client: ");
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}

#if HAVE_LIBXKBCOMMON
DEFINE_UNREF_CLEANUP_FUNC(xkb_context);
DEFINE_UNREF_CLEANUP_FUNC(xkb_keymap);
DEFINE_UNREF_CLEANUP_FUNC(xkb_state);
#endif

static void
setup_keymap(struct ei_device *kbd, const char *layout)
{
#if HAVE_LIBXKBCOMMON
	_unref_(xkb_context) *ctx = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!ctx)
		return;

	struct xkb_rule_names names = {
		.rules = "evdev",
		.model = "pc105",
		.layout = layout,
	};

	_unref_(xkb_keymap) *keymap = xkb_keymap_new_from_names(ctx, &names, 0);
	if (!keymap)
		return;

	const char *str = xkb_keymap_get_as_string(keymap, XKB_KEYMAP_FORMAT_TEXT_V1);
	size_t len = strlen(str) - 1;

	struct memfile *f = memfile_new(str, len);
	if (!f)
		return;

	_unref_(ei_keymap) *ei_keymap = ei_keymap_new(EI_KEYMAP_TYPE_XKB,
						      memfile_get_fd(f),
						      memfile_get_size(f));
	ei_device_keyboard_configure_keymap(kbd, ei_keymap);

	memfile_unref(f);
#endif
}

static void
handle_keymap(struct ei_event *event)
{
	struct ei_device *device = ei_event_get_device(event);

	if (!ei_device_has_capability(device, EI_DEVICE_CAP_KEYBOARD))
		return;

	const char *type = "none";
	const char *source = "unknown";
	struct ei_keymap *keymap = ei_device_keyboard_get_keymap(device);
	if (keymap) {
		switch (ei_keymap_get_type(keymap)) {
			case EI_KEYMAP_TYPE_XKB: type = "xkb"; break;
		}

		switch(ei_keymap_get_source(keymap)) {
		case EI_KEYMAP_SOURCE_CLIENT: source = "client"; break;
		case EI_KEYMAP_SOURCE_SERVER: source = "server"; break;
		}
	}

	colorprint("Using keymap type %s from the %s\n", type, source);
}

static void
usage(FILE *fp, const char *argv0)
{
	fprintf(fp,
		"Usage: %s [--verbose] [--socket|--portal] [--busname=a.b.c.d] [--layout=us]\n"
		"\n"
		"Start an EI demo client. The client will connect to EIS\n"
		"with the chosen backend (default: socket) and emulate pointer\n"
		"and keyboard events in a loop.\n"
		"\n"
		"Options:\n"
		" --socket	Use the socket backend. The socket path is $LIBEI_SOCKET if set, \n"
		"		otherwise XDG_RUNTIME/eis-0\n"
		" --portal	Use the portal backend.\n"
		" --busname     Use the given busname (default: org.freedesktop.portal.Desktop)\n"
		" --layout	Use the given XKB layout (requires libxkbcommon). Default: none\n"
		" --verbose	Enable debugging output\n"
		"",
		argv0);
}

int main(int argc, char **argv)
{
	enum {
		SOCKET,
		PORTAL,
	} backend = SOCKET;
	bool verbose = false;
	const char *layout = NULL;

	_cleanup_free_ char *busname = xstrdup("org.freedesktop.portal.Desktop");

	while (1) {
		enum {
			OPT_BACKEND_SOCKET,
			OPT_BACKEND_PORTAL,
			OPT_BUSNAME,
			OPT_LAYOUT,
			OPT_VERBOSE,
		};
		static struct option long_opts[] = {
			{"socket",	no_argument, 0, OPT_BACKEND_SOCKET},
			{ "portal",	no_argument, 0, OPT_BACKEND_PORTAL},
			{ "busname",	required_argument, 0, OPT_BUSNAME},
			{ "layout",	required_argument, 0, OPT_LAYOUT},
			{ "verbose",	no_argument, 0, OPT_VERBOSE},
			{ "help",	no_argument, 0, 'h'},
			{NULL},
		};

		int optind = 0;
		int c = getopt_long(argc, argv, "h", long_opts, &optind);
		if (c == -1)
			break;

		switch(c) {
			case 'h':
				usage(stdout, argv[0]);
				return EXIT_SUCCESS;
			case OPT_VERBOSE:
				verbose = true;
				break;
			case OPT_BACKEND_SOCKET:
				backend = SOCKET;
				break;
			case OPT_BACKEND_PORTAL:
				backend = PORTAL;
				break;
			case OPT_BUSNAME:
				free(busname);
				busname = xstrdup(optarg);
				break;
			case OPT_LAYOUT:
				layout = optarg;
				break;
			default:
				usage(stderr, argv[0]);
				return EXIT_FAILURE;
		}
	}

	_unref_(ei) *ei = ei_new(NULL);
	assert(ei);

	if (verbose)
		ei_log_set_priority(ei, EI_LOG_PRIORITY_DEBUG);

	ei_configure_name(ei, "ei-demo-client");

	int rc = -EINVAL;

	if (backend == SOCKET) {
		const char SOCKETNAME[] = "eis-0";
		colorprint("connecting to %s\n", SOCKETNAME);
		rc = ei_setup_backend_socket(ei, getenv("LIBEI_SOCKET") ? NULL : SOCKETNAME);
	} else if (backend == PORTAL) {
#if ENABLE_LIBEI_PORTAL
		colorprint("connecting to %s\n", busname);
		rc = ei_setup_backend_portal_busname(ei, busname);
#endif
	}

	if (rc != 0) {
		fprintf(stderr, "init failed: %s\n", strerror(errno));
		return 1;
	}

	struct pollfd fds = {
		.fd = ei_get_fd(ei),
		.events = POLLIN,
		.revents = 0,
	};
	_unref_(ei_device) *ptr = NULL;
	_unref_(ei_device) *kbd = NULL;

	bool stop = false;
	bool have_ptr = false;
	bool have_kbd = false;
	struct ei_seat *default_seat = NULL;

	while (!stop && poll(&fds, 1, 2000) > -1) {
		ei_dispatch(ei);

		while (!stop) {
			_unref_(ei_event) *e = ei_get_event(ei);
			if (!e)
				break;

			switch(ei_event_get_type(e)) {
			case EI_EVENT_CONNECT:
				colorprint("connected\n");
				break;
			case EI_EVENT_DISCONNECT:
				{
				colorprint("disconnected us\n");
				stop = true;
				break;
				}
			case EI_EVENT_SEAT_ADDED:
				{
				if (default_seat) {
					colorprint("ignoring other seats\n");
					break;
				}
				default_seat = ei_seat_ref(ei_event_get_seat(e));
				colorprint("seat added: %s\n", ei_seat_get_name(default_seat));
				ptr = ei_device_new(default_seat);
				ei_device_configure_capability(ptr, EI_DEVICE_CAP_POINTER);

				kbd = ei_device_new(default_seat);
				ei_device_configure_capability(kbd, EI_DEVICE_CAP_KEYBOARD);
				if (layout)
					setup_keymap(kbd, layout);

				ei_device_add(ptr);
				ei_device_add(kbd);
				break;
				}
			case EI_EVENT_SEAT_REMOVED:
				if (ei_event_get_seat(e) == default_seat)
					default_seat = ei_seat_unref(default_seat);
				break;
			case EI_EVENT_DEVICE_ADDED:
				colorprint("our device was accepted, waiting for resume\n");
				handle_keymap(e);
				break;
			case EI_EVENT_DEVICE_RESUMED:
				colorprint("our device was resumed\n");
				if (ei_event_get_device(e) == ptr)
					have_ptr = true;
				if (ei_event_get_device(e) == kbd)
					have_kbd = true;
				break;
			case EI_EVENT_DEVICE_SUSPENDED:
				colorprint("our device was suspended\n");
				if (ei_event_get_device(e) == ptr)
					have_ptr = false;
				if (ei_event_get_device(e) == kbd)
					have_kbd = false;
				break;
			case EI_EVENT_DEVICE_REMOVED:
				{
				colorprint("our device was removed\n");
				break;
				}
			default:
				abort();
			}
		}

		if (have_ptr) {
			colorprint("sending motion event\n");
			ei_device_pointer_motion(ptr, -1, 1);
			/* BTN_LEFT */
			colorprint("sending button event\n");
			ei_device_pointer_button(ptr, 0x110, true);
			ei_device_pointer_button(ptr, 0x110, false);
		}

		if (have_kbd) {
			static int key = 0;
			colorprint("sending key event\n");
			ei_device_keyboard_key(kbd, 16 + key, true); /* KEY_Q */
			ei_device_keyboard_key(kbd, 16 + key, false); /* KEY_Q */
			key = (key + 1) % 6;
		}
	}

	return 0;
}
