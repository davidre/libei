/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>

#include <munit.h>

#include "eierpecken.h"

#include "util-bits.h"
#include "util-mem.h"
#include "util-tristate.h"
#include "util-logger.h"
#include "util-object.h"
#include "util-strings.h"

DEFINE_TRISTATE(yes, no, unset);

struct peck {
	struct object object;
	struct ei *ei;
	struct eis *eis;
	uint32_t eis_behavior;
	uint32_t ei_behavior;
	struct logger *logger;

	struct eis_seat *eis_seat;
	struct ei_seat *ei_seat;

	struct eis_client *eis_client;

	uint32_t indent;
};

static const uint32_t INDENTATION = 4;
static void
peck_indent(struct peck *peck)
{
	peck->indent += INDENTATION;
}

static void
peck_dedent(struct peck *peck)
{
	assert(peck->indent >= INDENTATION);
	peck->indent -= INDENTATION;
}

static
OBJECT_IMPLEMENT_GETTER(peck, indent, uint32_t);

static void
peck_destroy(struct peck *peck)
{
	log_debug(peck, "destroying peck context\n");

	/* we don't want valgrind to complain about us not handling *all*
	 * events in a test */
	peck_drain_ei(peck->ei);
	peck_drain_eis(peck->eis);

	log_debug(peck, "final event processing done\n");

	eis_client_unref(peck->eis_client);

	ei_seat_unref(peck->ei_seat);
	eis_seat_unref(peck->eis_seat);

	ei_unref(peck->ei);
	eis_unref(peck->eis);
	logger_unref(peck->logger);
}

static
OBJECT_IMPLEMENT_CREATE(peck);
OBJECT_IMPLEMENT_UNREF(peck);
OBJECT_IMPLEMENT_GETTER(peck, ei, struct ei*);
OBJECT_IMPLEMENT_GETTER(peck, eis, struct eis*);

void
peck_drop_ei(struct peck *peck)
{
	peck->ei_seat = ei_seat_unref(peck->ei_seat);
	peck->ei = NULL;
}

struct eis_client *
peck_eis_get_default_client(struct peck *peck)
{
	munit_assert_ptr_not_null(peck->eis_client);
	return peck->eis_client;
};

struct eis_seat *
peck_eis_get_default_seat(struct peck *peck)
{
	munit_assert_ptr_not_null(peck->eis_seat);
	return peck->eis_seat;
};

struct ei_seat *
peck_ei_get_default_seat(struct peck *peck)
{
	munit_assert_ptr_not_null(peck->ei_seat);
	return peck->ei_seat;
}

static void
peck_ei_log_handler(struct ei *ei,
		    enum ei_log_priority priority,
		    const char *message,
		    bool is_continuation)
{
	if (is_continuation) {
		fprintf(stderr, "%s", message);
		return;
	}

	const char *prefix = NULL;
	switch (priority) {
		case EI_LOG_PRIORITY_ERROR:   prefix = "ERR"; break;
		case EI_LOG_PRIORITY_WARNING: prefix = "WRN"; break;
		case EI_LOG_PRIORITY_INFO:    prefix = "INF"; break;
		case EI_LOG_PRIORITY_DEBUG:   prefix = "DBG"; break;
	}

	struct peck *peck = ei_get_user_data(ei);
	fprintf(stderr, "|      | ei |     | %s |%*s🥚 %s",
		prefix, peck_get_indent(peck), " ", message);
}

static void
peck_eis_log_handler(struct eis *eis,
		    enum eis_log_priority priority,
		    const char *message,
		    bool is_continuation)
{
	if (is_continuation) {
		fprintf(stderr, "%s", message);
		return;
	}

	const char *prefix = NULL;
	switch (priority) {
		case EIS_LOG_PRIORITY_ERROR:   prefix = "ERR"; break;
		case EIS_LOG_PRIORITY_WARNING: prefix = "WRN"; break;
		case EIS_LOG_PRIORITY_INFO:    prefix = "INF"; break;
		case EIS_LOG_PRIORITY_DEBUG:   prefix = "DBG"; break;
	}

	struct peck *peck = eis_get_user_data(eis);
	fprintf(stderr, "|      |    | EIS | %s |%*s🍨 %s",
		prefix, peck_get_indent(peck), " ", message);
}

static void
peck_log_handler(struct logger *logger,
		 const char *prefix,
		 enum logger_priority priority,
		 const char *format, va_list args)
{
	const char *msgtype;

	switch(priority) {
	case LOGGER_DEBUG: msgtype = "DBG";  break;
	case LOGGER_INFO:  msgtype = "INF";   break;
	case LOGGER_WARN:  msgtype = "WRN";   break;
	case LOGGER_ERROR: msgtype = "ERR";  break;
	default:
		msgtype = "<invalid msgtype>";
		break;
	}

	struct peck *peck = logger_get_user_data(logger);

	fprintf(stderr, "| peck |    |     | %s |%*s",
		msgtype, peck_get_indent(peck), " ");
	vfprintf(stderr, format, args);
}

struct peck *
peck_new(void)
{
	struct peck *peck = peck_create(NULL);

	int sv[2];
	int rc = socketpair(AF_UNIX, SOCK_STREAM|SOCK_NONBLOCK|SOCK_CLOEXEC, 0, sv);
	munit_assert_int(rc, ==, 0);

	struct ei *ei = ei_new(peck);
	ei_set_user_data(ei, peck);
	ei_log_set_handler(ei, peck_ei_log_handler);
	ei_log_set_priority(ei, EI_LOG_PRIORITY_DEBUG);
	ei_configure_name(ei, "eierpecken test context");
	rc = ei_setup_backend_fd(ei, sv[0]);
	munit_assert_int(rc, ==, 0);
	peck->ei = ei;
	flag_set(peck->ei_behavior, PECK_EI_BEHAVIOR_AUTOCONNNECT);
	flag_set(peck->ei_behavior, PECK_EI_BEHAVIOR_AUTOSEAT);

	struct eis *eis = eis_new(peck);
	eis_log_set_handler(eis, peck_eis_log_handler);
	eis_log_set_priority(eis, EIS_LOG_PRIORITY_DEBUG);
	rc = eis_setup_backend_fd(eis);
	munit_assert_int(rc, ==, 0);
	rc = eis_backend_fd_add_fd(eis, sv[1]);
	munit_assert_int(rc, ==, 0);
	peck->eis = eis;
	peck->eis_behavior = PECK_EIS_BEHAVIOR_NONE;

	peck->logger = logger_new("peck", peck);
	logger_set_handler(peck->logger, peck_log_handler);
	logger_set_priority(peck->logger, LOGGER_DEBUG);

	return peck;
}

void
peck_enable_eis_behavior(struct peck *peck, enum peck_eis_behavior behavior)
{
	switch (behavior) {
	case PECK_EIS_BEHAVIOR_NONE:
		peck->eis_behavior = 0;
		break;
	case PECK_EIS_BEHAVIOR_DEFAULT_SEAT:
	case PECK_EIS_BEHAVIOR_NO_DEFAULT_SEAT:
		flag_set(peck->eis_behavior, behavior);
		break;
	case PECK_EIS_BEHAVIOR_ACCEPT_ALL:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_DEFAULT_SEAT);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_CLIENT);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_DEVICE);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_RESUME_DEVICE);
		break;
	case PECK_EIS_BEHAVIOR_REJECT_ALL:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_REJECT_CLIENT);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_REJECT_DEVICE);
		break;
	case PECK_EIS_BEHAVIOR_ACCEPT_DEVICE:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_POINTER);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_POINTER_ABSOLUTE);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_KEYBOARD);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_ACCEPT_TOUCH);
		break;
	case PECK_EIS_BEHAVIOR_REJECT_DEVICE:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_DROP_POINTER);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_DROP_POINTER_ABSOLUTE);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_DROP_KEYBOARD);
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_DROP_TOUCH);
		break;
	case PECK_EIS_BEHAVIOR_DROP_POINTER:
	case PECK_EIS_BEHAVIOR_DROP_POINTER_ABSOLUTE:
	case PECK_EIS_BEHAVIOR_DROP_KEYBOARD:
	case PECK_EIS_BEHAVIOR_DROP_TOUCH:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_HANDLE_DEVICE);
		_fallthrough_;
	case PECK_EIS_BEHAVIOR_REJECT_CLIENT:
		flag_clear(peck->eis_behavior, behavior - 1);
		flag_set(peck->eis_behavior, behavior);
		break;
	case PECK_EIS_BEHAVIOR_ACCEPT_POINTER:
	case PECK_EIS_BEHAVIOR_ACCEPT_POINTER_ABSOLUTE:
	case PECK_EIS_BEHAVIOR_ACCEPT_KEYBOARD:
	case PECK_EIS_BEHAVIOR_ACCEPT_TOUCH:
		peck_enable_eis_behavior(peck, PECK_EIS_BEHAVIOR_HANDLE_DEVICE);
		_fallthrough_;
	case PECK_EIS_BEHAVIOR_ACCEPT_CLIENT:
		flag_clear(peck->eis_behavior, behavior + 1);
		flag_set(peck->eis_behavior, behavior);
		break;
	case PECK_EIS_BEHAVIOR_HANDLE_DEVICE:
		flag_set(peck->eis_behavior, behavior);
		break;
	case PECK_EIS_BEHAVIOR_RESUME_DEVICE:
		flag_clear(peck->eis_behavior, PECK_EIS_BEHAVIOR_SUSPEND_DEVICE);
		flag_set(peck->eis_behavior, behavior);
		break;
	case PECK_EIS_BEHAVIOR_SUSPEND_DEVICE:
		flag_clear(peck->eis_behavior, PECK_EIS_BEHAVIOR_RESUME_DEVICE);
		flag_set(peck->eis_behavior, behavior);
		break;
	}
}

void
peck_enable_ei_behavior(struct peck *peck, enum peck_ei_behavior behavior)
{
	switch (behavior) {
	case PECK_EI_BEHAVIOR_NONE:
		peck->ei_behavior = 0;
		break;
	case PECK_EI_BEHAVIOR_AUTOCONNNECT:
	case PECK_EI_BEHAVIOR_AUTOSEAT:
		flag_set(peck->ei_behavior, behavior);
		break;
	case PECK_EI_BEHAVIOR_AUTODEVICES:
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_AUTOCONNNECT);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_AUTOSEAT);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_ADDED);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_RESUMED);
		break;
	case PECK_EI_BEHAVIOR_HANDLE_ADDED:
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER_ABSOLUTE);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_ADDED_KEYBOARD);
		peck_enable_ei_behavior(peck, PECK_EI_BEHAVIOR_HANDLE_ADDED_TOUCH);
		break;
	case PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER:
	case PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER_ABSOLUTE:
	case PECK_EI_BEHAVIOR_HANDLE_ADDED_KEYBOARD:
	case PECK_EI_BEHAVIOR_HANDLE_ADDED_TOUCH:
		flag_set(peck->ei_behavior, behavior);
		break;
	case PECK_EI_BEHAVIOR_HANDLE_RESUMED:
	case PECK_EI_BEHAVIOR_HANDLE_SUSPENDED:
		flag_set(peck->ei_behavior, behavior);
		break;
	}
}

static inline void
peck_create_eis_seat(struct peck *peck, struct eis_client *client)
{
	_unref_(eis_seat) *seat = eis_client_new_seat(client, "default");

	eis_seat_allow_capability(seat, EIS_DEVICE_CAP_POINTER);
	eis_seat_allow_capability(seat, EIS_DEVICE_CAP_POINTER_ABSOLUTE);
	eis_seat_allow_capability(seat, EIS_DEVICE_CAP_KEYBOARD);
	eis_seat_allow_capability(seat, EIS_DEVICE_CAP_TOUCH);

	log_debug(peck, "EIS adding seat: '%s'\n", eis_seat_get_name(seat));
	eis_seat_add(seat);

	peck->eis_seat = eis_seat_ref(seat);
}

static inline void
peck_handle_eis_connect(struct peck *peck, struct eis_event *e)
{
	struct eis_client *client = eis_event_get_client(e);

	if (flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_ACCEPT_CLIENT)) {
		log_debug(peck, "EIS accepting client: '%s'\n", eis_client_get_name(client));
		peck->eis_client = eis_client_ref(client);
		eis_client_connect(client);
		if (flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_DEFAULT_SEAT))
			peck_create_eis_seat(peck, client);
	} else {
		log_debug(peck, "EIS disconnecting client: '%s'\n", eis_client_get_name(client));
		eis_client_disconnect(client);
	}
}

static inline bool
peck_handle_eis_added(struct peck *peck, struct eis_event *e)
{
	struct eis_device *device = eis_event_get_device(e);
	uint32_t mask = 0;
	struct map {
		enum eis_device_capability cap;
		enum peck_eis_behavior behavior;
	} map[] = {
		{ EIS_DEVICE_CAP_POINTER, PECK_EIS_BEHAVIOR_ACCEPT_POINTER },
		{ EIS_DEVICE_CAP_POINTER_ABSOLUTE, PECK_EIS_BEHAVIOR_ACCEPT_POINTER_ABSOLUTE },
		{ EIS_DEVICE_CAP_KEYBOARD, PECK_EIS_BEHAVIOR_ACCEPT_KEYBOARD },
		{ EIS_DEVICE_CAP_TOUCH, PECK_EIS_BEHAVIOR_ACCEPT_TOUCH },
	};
	struct map *m;

	ARRAY_FOR_EACH(map, m) {
		if (eis_device_has_capability(device, m->cap)) {
			if (flag_is_set(peck->eis_behavior, m->behavior)) {
				mask |= bit(m->cap);
				eis_device_allow_capability(device, m->cap);
			} else {
				log_debug(peck, "Dropping cap %d\n", m->cap);
			}
		}
	}

	if (!mask) {
		log_debug(peck, "EIS refusing device: '%s'\n",
			  eis_device_get_name(device));
		eis_device_disconnect(device);
		return false;
	} else {
		log_debug(peck, "EIS adding device: '%s'\n",
			  eis_device_get_name(device));
		eis_device_connect(device);
		return true;
	}
}

bool
_peck_dispatch_eis(struct peck *peck, int lineno)
{
	struct eis *eis = peck->eis;
	bool had_event = false;

	log_debug(peck, "EIS Dispatch, line %d\n", lineno);
	peck_indent(peck);

	while (eis) {
		eis_dispatch(eis);
		tristate process_event = tristate_unset;

		_unref_(eis_event) *e = eis_peek_event(eis);
		if (!e)
			break;

		switch (eis_event_get_type(e)) {
		case EIS_EVENT_CLIENT_CONNECT:
		case EIS_EVENT_CLIENT_DISCONNECT:
			if (flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_ACCEPT_CLIENT) ||
			    flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_REJECT_CLIENT))
				process_event = tristate_yes;
			break;
		case EIS_EVENT_DEVICE_ADDED:
		case EIS_EVENT_DEVICE_REMOVED:
			if (flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_HANDLE_DEVICE))
				process_event = tristate_yes;
			else
				process_event = tristate_no;
			break;
		default:
			break;
		}

		log_debug(peck, "EIS received %s.... %s\n",
			  peck_eis_event_name(e),
			  tristate_is_yes(process_event) ?
				  "handling ourselves" : "punting to caller");

		if (!tristate_is_yes(process_event))
			break;

		had_event = true;

		/* manual unref, _cleanup_ will take care of the real event */
		eis_event_unref(e);
		e = eis_get_event(eis);

		switch (eis_event_get_type(e)) {
		case EIS_EVENT_CLIENT_CONNECT:
			peck_handle_eis_connect(peck, e);
			break;
		case EIS_EVENT_CLIENT_DISCONNECT:
			log_debug(peck, "EIS disconnecting client: %s\n",
				  eis_client_get_name(eis_event_get_client(e)));
			eis_client_disconnect(eis_event_get_client(e));
			break;
		case EIS_EVENT_DEVICE_ADDED:
			if (peck_handle_eis_added(peck, e) &&
			    flag_is_set(peck->eis_behavior, PECK_EIS_BEHAVIOR_RESUME_DEVICE))
				eis_device_resume(eis_event_get_device(e));
			break;
		case EIS_EVENT_DEVICE_REMOVED:
			log_debug(peck, "EIS removing device\n");
			eis_device_disconnect(eis_event_get_device(e));
			break;
		default:
			break;
		}
	}

	peck_dedent(peck);

	return had_event;
}

static inline tristate
peck_check_ei_added(struct peck *peck, struct ei_event *e)
{
	struct ei_device *device = ei_event_get_device(e);

	if (ei_device_has_capability(device, EI_DEVICE_CAP_POINTER) &&
	    flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER))
		return tristate_yes;

	if (ei_device_has_capability(device, EI_DEVICE_CAP_POINTER_ABSOLUTE) &&
	    flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_ADDED_POINTER_ABSOLUTE))
		return tristate_yes;

	if (ei_device_has_capability(device, EI_DEVICE_CAP_KEYBOARD) &&
	    flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_ADDED_KEYBOARD))
		return tristate_yes;

	if (ei_device_has_capability(device, EI_DEVICE_CAP_TOUCH) &
	    flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_ADDED_TOUCH))
		return tristate_yes;

	return tristate_unset;
}

bool
_peck_dispatch_ei(struct peck *peck, int lineno)
{
	struct ei *ei = peck->ei;
	bool had_event = false;

	log_debug(peck, "ei dispatch, line %d\n", lineno);
	peck_indent(peck);

	while (ei) {
		ei_dispatch(ei);
		tristate process_event = tristate_no;

		_unref_(ei_event) *e = ei_peek_event(ei);
		if (!e)
			break;

		switch (ei_event_get_type(e)) {
		case EI_EVENT_CONNECT:
			if (flag_is_set(peck->ei_behavior,
					PECK_EI_BEHAVIOR_AUTOCONNNECT))
				process_event = tristate_yes;
			break;
		case EI_EVENT_SEAT_ADDED:
			if (peck->ei_seat == NULL &&
			    flag_is_set(peck->ei_behavior,
					PECK_EI_BEHAVIOR_AUTOSEAT))
				process_event = tristate_yes;
			break;
		case EI_EVENT_DEVICE_ADDED:
			process_event = peck_check_ei_added(peck, e);
			break;
		case EI_EVENT_DEVICE_RESUMED:
			if (flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_RESUMED))
				process_event = tristate_yes;
			break;
		case EI_EVENT_DEVICE_SUSPENDED:
			if (flag_is_set(peck->ei_behavior, PECK_EI_BEHAVIOR_HANDLE_SUSPENDED))
				process_event = tristate_yes;
			break;
		default:
			break;
		}

		log_debug(peck, "ei event %s.... %s\n",
			  peck_ei_event_name(e),
			  tristate_is_yes(process_event) ?
				  "handling ourselves" : "punting to caller");

		if (!tristate_is_yes(process_event))
			break;

		had_event = true;

		/* manual unref, _cleanup_ will take care of the real event */
		ei_event_unref(e);
		e = ei_get_event(ei);

		switch (ei_event_get_type(e)) {
		case EI_EVENT_CONNECT:
			log_debug(peck, "ei is connected\n");
			/* Nothing to do here */
			break;
		case EI_EVENT_SEAT_ADDED:
			{
			struct ei_seat *seat = ei_event_get_seat(e);
			munit_assert_ptr_null(peck->ei_seat);
			peck->ei_seat = ei_seat_ref(seat);
			log_debug(peck, "default seat: %s\n");
			break;
			}
		case EI_EVENT_DEVICE_ADDED:
		case EI_EVENT_DEVICE_RESUMED:
		case EI_EVENT_DEVICE_SUSPENDED:
			/* Nothing to do here */
			break;
		default:
			break;
		}
	}

	peck_dedent(peck);
	return had_event;
}

void
_peck_dispatch_until_stable(struct peck *peck, int lineno)
{
	bool eis, ei;

	log_debug(peck, "dispatching until stable (line %d) {\n", lineno);
	peck_indent(peck);

	do {
		eis = _peck_dispatch_eis(peck, lineno);
		ei = _peck_dispatch_ei(peck, lineno);
	} while (ei || eis);

	peck_dedent(peck);
	log_debug(peck, "}\n", lineno);
}

void
peck_drain_eis(struct eis *eis)
{
	if (!eis)
		return;

	eis_dispatch(eis);

	while (true) {
		_unref_(eis_event) *e = eis_get_event(eis);
		if (e == NULL)
			break;
	}
}

void
peck_drain_ei(struct ei *ei)
{
	if (!ei)
		return;

	ei_dispatch(ei);
	while (true) {
		_unref_(ei_event) *e = ei_get_event(ei);
		if (e == NULL)
			break;
	}
}

void
peck_assert_no_ei_events(struct ei *ei)
{
	ei_dispatch(ei);
	while (true) {
		_unref_(ei_event) *e = ei_get_event(ei);
		if (!e)
			return;
		munit_errorf("Expected empty event queue, have: %s\n",
			     peck_ei_event_name(e));
	}
}

void
peck_assert_no_eis_events(struct eis *eis)
{
	eis_dispatch(eis);
	while (true) {
		_unref_(eis_event) *e = eis_get_event(eis);
		if (!e)
			return;
		munit_errorf("Expected empty event queue, have: %s\n",
			     peck_eis_event_name(e));
	}
}

struct ei_event *
_peck_ei_next_event(struct ei *ei, enum ei_event_type type, int lineno)
{
	struct ei_event *event = ei_get_event(ei);
	if (!event)
		munit_errorf("Expected ei event type %s, got none, line %d\n",
			     peck_ei_event_type_name(type),
			     lineno);
	if (!streq(peck_ei_event_name(event),
		   peck_ei_event_type_name(type)))
		munit_errorf("Expected ei event type %s, got %s, line %d\n",
			     peck_ei_event_type_name(type),
			     peck_ei_event_name(event),
			     lineno);

	return event;
}

struct eis_event *
_peck_eis_next_event(struct eis *eis, enum eis_event_type type, int lineno)
{
	struct eis_event *event = eis_get_event(eis);
	if (!event)
		munit_errorf("Expected EIS event type %s, got none, line %d\n",
			     peck_eis_event_type_name(type),
			     lineno);
	munit_assert_string_equal(peck_eis_event_name(event),
				  peck_eis_event_type_name(type));

	return event;
}

struct eis_event *
_peck_eis_touch_event(struct eis *eis, enum eis_event_type type, double x, double y, int lineno)
{
	assert(type == EIS_EVENT_TOUCH_DOWN || type == EIS_EVENT_TOUCH_MOTION);

	struct eis_event *event = _peck_eis_next_event(eis, type, lineno);
	double ex = eis_event_touch_get_x(event);
	double ey = eis_event_touch_get_y(event);

	if (fabs(ex - x) > 1e-3 || fabs(ey - y) > 1e-3) {
		munit_errorf("Touch coordinate mismatch: have (%.2f/%.2f) need (%.2f/%.2f)\n",
			     ex, ey, x, y);
	}

	return event;
}

const char *
peck_ei_event_name(struct ei_event *e)
{
	return peck_ei_event_type_name(ei_event_get_type(e));
}

const char *
peck_ei_event_type_name(enum ei_event_type type)
{
#define CASE_STRING(_name) \
	case EI_EVENT_##_name: return #_name

	switch (type) {
		CASE_STRING(CONNECT);
		CASE_STRING(DISCONNECT);
		CASE_STRING(SEAT_ADDED);
		CASE_STRING(SEAT_REMOVED);
		CASE_STRING(DEVICE_ADDED);
		CASE_STRING(DEVICE_REMOVED);
		CASE_STRING(DEVICE_SUSPENDED);
		CASE_STRING(DEVICE_RESUMED);
		default:
			assert(!"Unhandled ei event type");
	}
#undef CASE_STRING
}

const char *
peck_eis_event_name(struct eis_event *e)
{
	return peck_eis_event_type_name(eis_event_get_type(e));
}

const char *
peck_eis_event_type_name(enum eis_event_type type)
{
#define CASE_STRING(_name) \
	case EIS_EVENT_##_name: return #_name

	switch (type) {
		CASE_STRING(CLIENT_CONNECT);
		CASE_STRING(CLIENT_DISCONNECT);
		CASE_STRING(DEVICE_ADDED);
		CASE_STRING(DEVICE_REMOVED);
		CASE_STRING(POINTER_MOTION);
		CASE_STRING(POINTER_MOTION_ABSOLUTE);
		CASE_STRING(POINTER_BUTTON);
		CASE_STRING(KEYBOARD_KEY);
		CASE_STRING(TOUCH_DOWN);
		CASE_STRING(TOUCH_UP);
		CASE_STRING(TOUCH_MOTION);
		default:
			assert(!"Unhandled EIS event type");
	}
#undef CASE_STRING
}
